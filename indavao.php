<?php
/*
Plugin Name: INDAVAO.NET Plugin
Plugin URI: http://indavao.net
Description: InDavao.Net Wordpress Site
Version: 1.0
Author: Chester Alan
Author URI: http://www.chesteralan.com
*/

// set plugin dir
$plugin_dir = 'indavao';

// required libraries
require('lib/lib.includes.php');

// Main Features
require('content.php');
require('networking.php');
require('marketing.php');
require('collections.php');
require('management.php');

// others
require('inc/common/all.php');


