<?php

if (! class_exists( 'Custom_Metabox' )) {
    class Custom_Metabox
    {
        var $id;
        var $title;
        var $post_type;
        var $context='normal'; // ('normal', 'advanced', or 'side')
        var $priority='default'; // ('high', 'core', 'default' or 'low')
        var $fields = array();
        var $tabs = array();
        
        function __construct($post_type, $id, $title, $context='normal', $priority='default') {
            $this->id = $id;
            $this->title = $title;
            $this->post_type = $post_type;
            $this->context = $context;
            $this->priority = $priority;
            return $this;
        }    
        
        function init()
        {
			add_action('admin_head', array(&$this,'add_header_js'));
            add_action('add_meta_boxes', array(&$this,'add_meta_box'));
            add_action('save_post', array(&$this, 'save_post'));
            add_action('admin_footer', array(&$this,'add_footer_js'));
        }
        
        function add_meta_box() {
                add_meta_box($this->id, $this->title, array(&$this, 'meta_box_content'), $this->post_type, $this->context, $this->priority);
        }
        
        function meta_box_content()
        {
            global $post;
            echo '<input type="hidden" name="custom_meta_box_nonce_'.$post->ID.'" value="'.wp_create_nonce( 'custom_meta_box_nonce_'. $post->ID ).'" />';
            
            if( count($this->tabs) > 0 ) {
                echo '<div class="metabox-tabs"><ul>';
                    foreach($this->tabs as $tab) {
                        echo '<li '.$default.'><a href="#'.$tab['id'].'">'.$tab['label'].'</a></li>';
                    }
                echo '</ul>';
                
                foreach($this->tabs as $tab) {
                    echo '<div id="'.$tab['id'].'">';
                        if( count($this->fields) > 0) {
                            foreach($this->fields as $field) {
                                if( isset( $field['tab'] ) && $field['tab'] == $tab['id'] ) {
                                    $this->show_field($field, $post->ID);
                                }
                            }
                        }
                    echo '</div>';                    
                }
                
                echo '</div>';
            } else {
                if( count($this->fields) > 0) {
                    foreach($this->fields as $field) {
                        $this->show_field($field, $post->ID);
                    }
                }
            }
       
        }
        
        function add_tab( Array $tab ) {
        
            $defaults = array(
                'id' => '',
                'label' => ''
            );
            
            $this->tabs[] = array_merge($defaults, $tab);
        }
        
        function add_field(Array $field) {
            $defaults = array(
                'id' => '',
                'label' => '',
                'desc' => '',
                'options' => array(),
                'default' => '',
				'styles'=>'',
				'placeholder'=>'',
				'textarea-cols'=>'60',
				'textarea-rows'=>'4',
				'text-size'=>'30',
				'slider-size'=>'5',
				'select-selected'=>'',
				'update-button'=>false,
				'update-callback' => '',
            );
            $this->fields[] =  array_merge($defaults, $field );
        }
        
        function show_field(Array $field, $id) {
            $current_value = get_post_meta($id, $field['id'], true);
            switch($field['type']) {                    
					// text
					case 'text':
						$current_value = ($current_value!='') ? $current_value : $field['default'];
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						if( $field['update-button'] ) { echo '<button class="button button-primary button-large" data-id="'.$field['id'].'" type="button" style="float:right;" id="'.$field['id'].'_update">'.$field['update-button'].'</button>'; }
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="'.$field['text-size'].'" style="'.$field['styles'].'" placeholder="'.$field['placeholder'].'" />
								<br><span class="description">'.$field['desc'].'</span>';
					break;
					// text
					case 'email':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<input type="email" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="'.$field['text-size'].'" style="'.$field['styles'].'" placeholder="'.$field['placeholder'].'" />
								<span class="description">'.$field['desc'].'</span>';
					break;
					// textarea
					case 'textarea':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="'.$field['textarea-cols'].'" rows="'.$field['textarea-rows'].'" style="'.$field['styles'].'" placeholder="'.$field['placeholder'].'">'.$current_value.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// hidden
					case 'hidden':
						echo '<input type="hidden" name="'.$field['id'].'" value="'.$field['default'].'" />';
						break;
					// select
					case 'select':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'" style="'.$field['styles'].'">';
						foreach ($field['options'] as $option) {
							$selected_value = ($current_value!='') ? $current_value : $field['default'];
							echo '<option', ($option['value'] == $selected_value) ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ', $current_value ? ' checked="checked"' : '','/>
								<label for="'.$field['id'].'">'.$field['label'].'</label>';
						echo '<p><em><small class="description">'.$field['desc'].'</small></em></p>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$current_value == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"',$current_value && in_array($option['value'], $current_value) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['id'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['id']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['id']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
					    $items = get_posts( array (
						    'post_type'	=> $field['post_type'],
						    'posts_per_page' => -1
					    ));
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$current_value == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="'.$field['text-size'].'" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'datetime':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<input type="text" class="datetimepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$current_value.'" size="'.$field['text-size'].'" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						$value = $current_value != '' ? $current_value : '0';
						echo '<div id="'.$field['id'].'-slider"></div>
								<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" size="'.$field['slider-size'].'" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						$image = get_template_directory_uri().'/images/image.png';	
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($current_value) { $image = wp_get_attachment_image_src($current_value, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$current_value.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					case 'post-thumbnail':

// Get WordPress' media upload URL
$upload_link = esc_url( get_upload_iframe_src( $this->post_type, $id ) );

// Get the image src
$your_img_src = wp_get_attachment_image_src( $current_value, 'full' );

// For convenience, see if the array is valid
$you_have_img = is_array( $your_img_src );
?>

<!-- Your image container, which can be manipulated with js -->
<div class="<?php echo $field['id']; ?>-container">
    <?php if ( $you_have_img ) : ?>
        <img src="<?php echo $your_img_src[0]; ?>" alt="" style="max-width:100%;" />
    <?php endif; ?>
</div>

<!-- Your add & remove image links -->
<p class="hide-if-no-js">
    <a class="upload-<?php echo $field['id']; ?> <?php if ( $you_have_img  ) { echo 'hidden'; } ?>" 
       href="<?php echo $upload_link; ?>">
        <?php _e($field['label']); ?>
    </a>
    <a class="delete-<?php echo $field['id']; ?> <?php if ( ! $you_have_img  ) { echo 'hidden'; } ?>" 
      href="#">
        <?php _e('Remove this image'); ?>
    </a>
</p>

<!-- A hidden input to set and post the chosen image id -->
<input class="<?php echo $field['id']; ?>" name="<?php echo $field['id']; ?>" type="hidden" value="<?php echo esc_attr( $current_value ); ?>" />
<?php
						
					break;
					// repeatable
					case 'repeatable':
						echo ($field['label'] != '') ? '<p><label for="'.$field['id'].'">'.$field['label'].'</label><br>' : '';
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($current_value) {
							foreach($current_value as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="'.$row.'" size="'.$field['text-size'].'" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="" size="'.$field['text-size'].'" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
					// html
					case 'desc':
						echo $field['desc'];
						break;
					
				} //end switch
				echo '</p>';
        }
        
        function save_post( $post_id ) {
	        global $post;

	        // verify nonce
	        if ( ! wp_verify_nonce( $_POST[ 'custom_meta_box_nonce_'. $post_id ], 'custom_meta_box_nonce_'. $post_id ) ) 
		        return $post_id;
		 
	        // check autosave
	        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		        return $post_id;
		
	        // check permissions
	        if ('page' == $_POST['post_type']) {
		        if (!current_user_can('edit_page', $post_id)) {
			        return $post_id;
		        } elseif (!current_user_can('edit_post', $post_id)) {
			        return $post_id; 
			        }
	        }
	
	

		        if( $this->post_type == $post->post_type ) {
			

			        foreach ($this->fields as $field) {
				        if($field['type'] == 'tax_select') continue;

				        $old = get_post_meta($post_id, $field['id'], true);
				
				        $new = $_POST[$field['id']];

				        if ($new && $new != $old) {
					        update_post_meta($post_id, $field['id'], $new);
				        } elseif ('' == $new && $old) {
					        delete_post_meta($post_id, $field['id'], $old);
				        }
			        }
		        }	

        }     
        
        function add_header_js() {
			$screen = get_current_screen();
			if( $this->post_type == $screen->post_type && $screen->base == 'post') {
foreach( $this->fields as $field ) {
	if( $field['type'] == 'post-thumbnail' ) {
		//print_r( $field );
echo <<<JS
<script type="text/javascript">
jQuery(function($){

  // Set all variables to be used in scope
  var frame,
      metaBox = $('#{$this->id}.postbox'), // Your meta box id here
      addImgLink = metaBox.find('.upload-{$field['id']}'),
      delImgLink = metaBox.find( '.delete-{$field['id']}'),
      imgContainer = metaBox.find( '.{$field['id']}-container'),
      imgIdInput = metaBox.find( '.{$field['id']}' );
  
  // ADD IMAGE LINK
  addImgLink.on( 'click', function( event ){
    
    event.preventDefault();
    
    // If the media frame already exists, reopen it.
    if ( frame ) {
      frame.open();
      return;
    }
    
    // Create a new media frame
    frame = wp.media({
      title: 'Select or Upload Media Of Your Chosen Persuasion',
      button: {
        text: 'Use this media'
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });

    
    // When an image is selected in the media frame...
    frame.on( 'select', function() {
      
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      // Send the attachment URL to our custom image input field.
      imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );

      // Send the attachment id to our hidden input
      imgIdInput.val( attachment.id );

      // Hide the add image link
      addImgLink.addClass( 'hidden' );

      // Unhide the remove image link
      delImgLink.removeClass( 'hidden' );
    });

    // Finally, open the modal on click
    frame.open();
  });
  
  
  // DELETE IMAGE LINK
  delImgLink.on( 'click', function( event ){

    event.preventDefault();

    // Clear out the preview image
    imgContainer.html( '' );

    // Un-hide the add image link
    addImgLink.removeClass( 'hidden' );

    // Hide the delete image link
    delImgLink.addClass( 'hidden' );

    // Delete the image id from the hidden input
    imgIdInput.val( '' );

  });

});
</script>
JS;
	}
}
			}
		}   
        
		function add_footer_js() {
			$screen = get_current_screen();
			if( $this->post_type == $screen->post_type && $screen->base == 'post') {
				foreach( $this->fields as $field ) {
					if( $field['update-callback'] !== '' ) {
echo <<<JS
<script type="text/javascript">
(function($){
	{$field['update-callback']}
})(jQuery);
</script>
JS;
					}
				}
			}
		}
    }
}
