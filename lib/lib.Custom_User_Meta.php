<?php

if (! class_exists( 'Custom_User_Meta' )) {

	class Custom_User_Meta {
	   	  
	   var $fields = array();
	   var $label = 'Label Here';
	   var $show_in_profile = true;
	   var $edit_user_profile = true;
	   
	   function __construct($show_in_profile=true, $edit_user_profile=true) {
		   $this->show_in_profile = $show_in_profile;
		   $this->edit_user_profile = $edit_user_profile;
	   }
	   
	   function init() {
			if( $this->show_in_profile ) {
				add_action( 'show_user_profile', array(&$this, 'user_profile') );
				add_action( 'personal_options_update', array(&$this, 'user_profile_update') );
			}
			if( $this->edit_user_profile ) {
				add_action( 'edit_user_profile', array(&$this, 'user_profile') );
				add_action( 'edit_user_profile_update', array(&$this, 'user_profile_update') );
			}
		}
		
		function user_profile( $userdata ) {
			echo '<h3>'.$this->label.'</h3>';
			echo '<table class="form-table">';
			foreach($this->fields as $field) {
				$this->show_field($field, $userdata->data->ID);
			}
			echo '</table>';
		}
		 
		function user_profile_update( $user_id ) {
			if ( !current_user_can( 'edit_user', $user_id ) ) {
				return false;
			}

			foreach($this->fields as $field) {
				update_user_meta( $user_id, $field['meta_key'], $_POST[$field['meta_key']] );
			}
		}
		
		function set_label( $value ) {
			if( $value != '') {
				$this->label = $value;
			}
			return $this;
		}
		
		function add_field(Array $field) {
			$defaults = array('label' => 'New Input', 'meta_key'=> '', 'meta_value'=> '', 'desc'=>'', 'type' => 'text', 'add_label' => true);
			$this->fields[] = array_merge($defaults, $field);
			return $this;
		}
		
		function show_field(Array $field, $id) {
            $current_value = get_user_meta($id, $field['meta_key'], TRUE);
            
            if( $current_value == '' ) {
                $current_value = $field['meta_value'];
            }
            echo '<tr class="'.$field['meta_key'].'">';
            if( $field['add_label'] ) {
                        echo '<th><label for="'.$field['meta_key'].'">'.$field['label'].'</label></th>
                        <td>';
            } else {
				echo '<td colspan="2">';
			}
                        
            switch($field['type']) {
					// show-value
					case 'show-value':
						echo $current_value;
					break;
					// hidden
					case 'hidden':
						echo '<input type="hidden" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$field['meta_value'].'" size="30" />' . $field['meta_value'];
					break;
					// text
					case 'text':
						echo '<input type="text" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// text
					case 'text_media':
$image_library_url = get_upload_iframe_src( 'image', null, 'library' );
$image_library_url = remove_query_arg( array('TB_iframe'), $image_library_url );
$image_library_url = add_query_arg( array( 'context' => 'shiba-gallery-default-image', 'TB_iframe' => 1 ), $image_library_url );

						echo '<input type="text" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
							<a href="'. esc_url( $image_library_url ) .'" id="'.$field['meta_key'].'_button" class="button thickbox">Open Media</a>
								<br /><span class="description">'.$field['desc'].'</span>';



echo <<<HTML
<script type="text/javascript">
    var win = window.dialogArguments || opener || parent || top; 
	win.send_to_editor = function( html ) {
		var patt = /src=[\"'](.*)[\"'] alt=/ig;
		var res = patt.exec( html );
		if(typeof res[1] !== 'undefined'){
			win.jQuery('#{$field['meta_key']}').val( res[1] );
		}
		win.tb_remove();
	}
    </script>
HTML;
								
					break;
					// textarea
					case 'textarea':
						echo '<textarea name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" cols="60" rows="4">'.$current_value.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" ',$current_value ? ' checked="checked"' : '','/>
								<label for="'.$field['meta_key'].'">'.$field['desc'].'</label>';
					break;
					// select
					case 'select':
						echo '<select name="'.$field['meta_key'].'" id="'.$field['meta_key'].'">';

						foreach ($field['options'] as $option) {
							echo '<option', $current_value == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['meta_key'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$current_value == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['meta_key'].'[]" id="'.$option['value'].'"',$current_value && in_array($option['value'], $current_value) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo '<select name="'.$field['meta_key'].'" id="'.$field['meta_key'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['meta_key'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['meta_key']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['meta_key']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['meta_key'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
					$items = get_posts( array (
						'post_type'	=> $field['post_type'],
						'posts_per_page' => -1
					));
						echo '<select name="'.$field['meta_key'].'" id="'.$field['meta_key'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$current_value == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo '<input type="text" class="datepicker" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'datetime':
						echo '<input type="text" class="datetimepicker" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$current_value.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
					$value = $current_value != '' ? $current_value : '0';
						echo '<div id="'.$field['meta_key'].'-slider"></div>
								<input type="text" name="'.$field['meta_key'].'" id="'.$field['meta_key'].'" value="'.$value.'" size="5" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						$image = get_template_directory_uri().'/images/image.png';	
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($current_value) { $image = wp_get_attachment_image_src($current_value, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['meta_key'].'" type="hidden" class="custom_upload_image" value="'.$current_value.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">Remove Image</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// repeatable
					case 'repeatable':
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['meta_key'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($current_value) {
							foreach($current_value as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['meta_key'].'['.$i.']" id="'.$field['meta_key'].'" value="'.$row.'" size="30" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['meta_key'].'['.$i.']" id="'.$field['meta_key'].'" value="" size="30" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
					case 'html':
					echo $field['html'];
					break;
				} //end switch
				echo '</td>
    </tr>';
        }
		
	}

}
