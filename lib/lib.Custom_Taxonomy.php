<?php

if (! class_exists( 'Custom_Taxonomy' )) {

class Custom_Taxonomy 
    {
        var $id;
        var $name;
        var $menu_name;
        var $plural;
        var $post_type;
        var $hierarchical = true;
        var $slug; 
        var $show_in_nav_menus = true;
        
        function __construct( $post_type, $id='', $name='', $menu_name='', $plural='', $hierarchical=true) {
            $this->id = $id;
            $this->slug = $id;
            $this->name = $name;
            $this->menu_name = $menu_name;
            $this->plural = $plural;
            $this->post_type = $post_type;
            $this->hierarchical = $hierarchical;
        }    
        
        function init()
        {
            if( $this->id && $this->post_type ) {
                add_action( 'init', array(&$this, 'register_taxonomy' ) );
            }
        }
        
        function register_taxonomy()
        {
            $args_property_type = array(
		    'hierarchical'          => $this->hierarchical, 
		    'labels'                => array(
				'name'                       => _x( $this->plural, 'taxonomy general name' ),
				'singular_name'              => _x( $this->name, 'taxonomy singular name' ),
				'search_items'               => __( 'Search ' . $this->name ),
				'popular_items'              => __( 'Popular ' . $this->plural ),
				'all_items'                  => __( 'All ' . $this->plural ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit ' . $this->name ),
				'update_item'                => __( 'Update ' . $this->name ),
				'add_new_item'               => __( 'Add New ' . $this->name ),
				'new_item_name'              => __( 'New ' .$this->name .' Name' ),
				'separate_items_with_commas' => __( 'Separate ' .$this->name. ' with commas' ),
				'add_or_remove_items'        => __( 'Add or remove ' . $this->name ),
				'choose_from_most_used'      => __( 'Choose from the most used ' . $this->name ),
				'not_found'                  => __( 'No '.$this->name.' found.' ),
				'menu_name'                  => __( $this->menu_name ),
					),
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => $this->slug ),
		'show_in_nav_menus' 	=> $this->show_in_nav_menus,
	);
	
            if( get_taxonomies( array( 'name' => $this->id ) ) ) {
                register_taxonomy_for_object_type(  $this->id, $this->post_type );
            } else {
	            register_taxonomy( $this->id, $this->post_type, $args_property_type );
	        }
	
       }
        
       function set_id($value) {
            $this->id = $value;
            return $this;
       }
       
       function set_slug($value) {
		    $this->slug = ($value!='') ? $value : $this->id;
            return $this;
	   }
	   
       function set_hierarchical( $value ) {
            $this->hierarchical = (bool) $value;
            return $this;
       }
       
       function set_name($value) {
            $this->name = $value;
            return $this;
       }
       
       function set_menu_name($value) {
            $this->menu_name = $value;
            return $this;
       }
       
       function set_plural($value) {
            $this->plural = $value;
            return $this;
       }
       
       function set_post_type($value) {
            $this->post_type = $value;
            return $this;
       }  
       
    }
}
