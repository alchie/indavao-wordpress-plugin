<?php

if (! class_exists( 'Custom_Sub_Admin_Page' ) && class_exists( 'Custom_Page' )) {
	class Custom_Sub_Admin_Page extends Custom_Page {
		public $id = 'custom_sub_admin_page';
		public $title = 'Custom Sub Admin Page';
		public $menu_name = 'Sub Admin Menu';
		public $permission = 'manage_options';
		public $admin_bar = false;
		public $priority = '9500';
		public $parent;
		public $admin_init = false;
		public $admin_head = false;
		public $admin_footer = false;
		public $admin_enqueue_scripts = false;
		
		function __construct(Custom_Admin_Page $parent) {
			$this->parent = $parent;
			add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
			if( $this->admin_bar ) {
				add_action( 'admin_bar_menu', array( &$this, 'admin_bar_menu'), $this->priority );
			}
			if( $this->admin_init ) {
				add_action( 'admin_init', array( &$this, 'admin_init' ));
			}
			if( $this->admin_head ) {
				add_action( 'admin_head', array( &$this, 'admin_head' ));
			}
			if( $this->admin_footer ) {
				add_action( 'admin_footer', array( &$this, 'admin_footer' ));
			}
			if( $this->admin_enqueue_scripts ) {
				add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue_scripts' ));
			}
		}
		
		function admin_menu() {
			add_submenu_page( $this->parent->id, $this->title, $this->menu_name, $this->parent->permission, $this->id, array( &$this, 'admin_page') );
		}
		
		function admin_bar_menu( $wp_admin_bar ) {
			$wp_admin_bar->add_node( array(
				'id' => 'sub_admin_bar_menu_' . $this->id,
				'title' => $this->menu_name,
				'parent' => 'admin_bar_menu_'.$this->parent->id,
				'href' => admin_url('admin.php?page='. $this->id)
			));
		}
		
		function admin_page() {
echo <<<HTML
<div class="wrap">
	<h2>{$this->title}</h2>
</div>
HTML;
		}
		
	}
}
