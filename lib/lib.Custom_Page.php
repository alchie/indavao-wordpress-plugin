<?php

if (! class_exists( 'Custom_Page' )) {
	class Custom_Page {
		
		public $id;
		public $title;
		
		function notification($text, $type="", $desc="") {
echo <<<NOTICE
<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
<p><strong>{$text}</strong> {$desc}</p>
<button type="button" class="notice-dismiss">
	<span class="screen-reader-text">Dismiss this notice.</span>
</button></div>
NOTICE;
		}
		
		function is_this_page() {
			if( isset( $_GET['page'] ) && $_GET['page'] == $this->id ) {
				return true;
			} else {
				return false;
			}
		}
		
		function not_this_page() {
			if( ! isset( $_GET['page'] ) || $_GET['page'] != $this->id ) {
				return true;
			} else {
				return false;
			}
		}
		
		function admin_menu() {}
		function admin_bar_menu() {}
		function admin_page() {}
		function admin_init() {}
		function admin_head() {}
		function admin_footer() {}
		function admin_enqueue_scripts() {}
	}
}
