<?php

if (! class_exists( 'Custom_Admin_Page' ) && class_exists( 'Custom_Page' )) {
	class Custom_Admin_Page extends Custom_Page {
		public $id = 'custom_admin_page';
		public $title = 'Custom Admin Page';
		public $menu_name = 'Admin Menu';
		public $permission = 'manage_options';
		public $icon = '';
		public $priority = '9000';
		public $admin_bar = false;
		public $admin_init = false;
		public $admin_head = false;
		public $admin_footer = false;
		public $admin_enqueue_scripts = false;
		
		function __construct() {
			add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
			if( $this->admin_bar ) {
				add_action( 'admin_bar_menu', array( &$this, 'admin_bar_menu'), $this->priority );
			}
			if( $this->admin_init ) {
				add_action( 'admin_init', array( &$this, 'admin_init' ));
			}
			if( $this->admin_head ) {
				add_action( 'admin_head', array( &$this, 'admin_head' ));
			}
			if( $this->admin_footer ) {
				add_action( 'admin_footer', array( &$this, 'admin_footer' ));
			}
			if( $this->admin_enqueue_scripts ) {
				add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue_scripts' ));
			}
		}
		
		function admin_menu() {
			global $plugin_dir;
			add_menu_page( $this->title, $this->menu_name, $this->permission, $this->id, array( &$this, 'admin_page'), plugins_url( $plugin_dir .'/images/' . $this->icon ), $this->priority );
		}
		
		function admin_bar_menu( $wp_admin_bar ) {
			$wp_admin_bar->add_node( array(
				'id' => 'admin_bar_menu_' . $this->id,
				'title' => $this->menu_name,
				'href' => admin_url('admin.php?page='. $this->id)
			));
		}
		
		function admin_page() {
echo <<<HTML
<div class="wrap">
	<h2>{$this->title}</h2>
</div>
HTML;
		}
		
	}
}
