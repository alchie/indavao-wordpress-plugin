<?php



// tools
require('inc/tools/marketing_tools.php');
require('inc/tools/sms_all_contacts.php');
require('inc/tools/sequential_sms_sender.php');
require('inc/tools/fb_auto_chat_friends.php');
require('inc/tools/fb_group_auto_joiner.php');
require('inc/tools/fb_group_auto_poster.php');
require('inc/tools/fb_page_auto_liker.php');
require('inc/tools/fb_page_auto_poster.php');

// marketing tools
$marketing_tools = new InDavaoMarketing();
$smsac = new InDavaoMarketingSMSAC($marketing_tools);
$ssmss = new InDavaoMarketingSSMSS($marketing_tools);
$fbacf = new InDavaoMarketingFBACF($marketing_tools);
$fbgaj = new InDavaoMarketingFBGAJ($marketing_tools);
$fbgap = new InDavaoMarketingFBGAP($marketing_tools);
$fbpal = new InDavaoMarketingFBPAL($marketing_tools);
$fbpap = new InDavaoMarketingFBPAP($marketing_tools);

// my referrals
require('inc/marketing/my_referrals.php');
require('inc/marketing/links_redirections.php');
require('inc/marketing/squeeze_pages.php');

// join now
require('inc/marketing/join_now.php');


$my_referrals = new InDavaoMarketingMyReferrals();
$referral_links = new InDavaoMarketingLinksRedirection($my_referrals);
$squeeze_pages = new InDavaoMarketingSqueezePages($my_referrals);

$join_now = new InDavaoMarketingJoinNow();

