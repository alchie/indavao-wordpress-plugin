<?php

add_action('init', 'referrals_redirects');
function referrals_redirects() {
	if( isset($_GET['ref']) && $_GET['ref'] != '' ) {
		  setcookie('indavao_sponsor_id', $_GET['ref'], (time()+60*60*24*30));
		  
		if( isset($_GET['redir']) && $_GET['redir'] != '' ) {
			$default = get_user_meta($_GET['ref'], '_indavao_user_referral_redirection_default', true);
			$redir = get_user_meta($_GET['ref'], '_indavao_user_referral_redirection_'.$_GET['redir'], true);
			$location = false;
			if( $redir != '') {
				$location = $redir;
			} else {
				if($default != '') {
					$location = $default;
				}
			}
			if( $location ) {
				header("location: " . $location);
				exit;
			}
		}
		if( isset($_GET['squeeze']) && $_GET['squeeze'] != '' ) {
			$pages = get_option('_indavao_squeeze_pages');
			if( $pages ) {
				$squeeze = $pages[$_GET['squeeze']];
				if( ($squeeze!=NULL) && ($squeeze['page_url'] != '')) {
					header("location: " . $squeeze['page_url']);
					exit;
				}
			}
		}
	}
}
