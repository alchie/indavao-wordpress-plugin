<?php

add_filter('login_headerurl', 'indavao_login_headerurl');
function indavao_login_headerurl() {
	return site_url();
}

add_filter('login_headertitle', 'indavao_login_headertitle');
function indavao_login_headertitle() {
	return get_bloginfo('name');
}

add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo() { 
	global $plugin_dir;
	$url = plugins_url($plugin_dir.'/images/logo256.png');
echo <<<S
    <style type="text/css">
		#login {
		  width: 400px;
		  padding: 5% 0 5%;
		  margin: auto;
		}
		#reg_passmail {
			display:none;
		}
        .login h1 a {
            background-image: url({$url});
        }
    </style>
S;
}


class InDavaoNetworkRegistration {
	function __construct() {
		add_action('register_form', array(&$this,'register_form'));
		add_action('login_head', array(&$this,'set_sponsor'));
		add_filter('login_message', array(&$this,'show_sponsor'));
		add_filter( 'registration_errors', array(&$this,'registration_errors'), 10, 3 );
		add_action( 'user_register', array(&$this,'user_register') );
		add_action('admin_init', array( &$this, 'user_meta'));
	}
	
	function register_form() {
	$user_firstname = ( ! empty( $_POST['user_firstname'] ) ) ? trim( $_POST['user_firstname'] ) : '';
	$first_name = esc_attr( wp_unslash( $user_firstname ) );
	
	$user_lastname = ( ! empty( $_POST['user_lastname'] ) ) ? trim( $_POST['user_lastname'] ) : '';
	$last_name = esc_attr( wp_unslash( $user_lastname ) );
	
	$user_phone = ( ! empty( $_POST['user_phone'] ) ) ? trim( $_POST['user_phone'] ) : '';
	$phone = esc_attr( wp_unslash( $user_phone ) );

$sponsor_id = 1;
if( isset($_REQUEST['sponsor']) && $_REQUEST['sponsor'] !== '') {
	$user = get_userdata( $_REQUEST['sponsor'] );
	$sponsor_id = $user->ID;
} else {
	if( isset($_COOKIE['indavao_sponsor_id']) && $_COOKIE['indavao_sponsor_id'] !== '') {
		$user = get_userdata( $_COOKIE['indavao_sponsor_id'] );
		$sponsor_id = $user->ID;
	} else {
		$sponsor_id = (get_option( '_indavao_default_sponsor_id') != '') ? get_option( '_indavao_default_sponsor_id') : 1;		
	}
}

// terms and conditions
$tac = get_option( '_indavao_terms_and_conditions');

echo '<input type="hidden" name="sponsor" value="'.$sponsor_id.'">';
echo <<<FORM
<p>
		<label for="user_firstname">First Name<br>
		<input type="text" name="user_firstname" id="user_firstname" class="input" value="{$first_name}" size="20"></label>
	</p>
<p>
		<label for="user_lastname">Last Name<br>
		<input type="text" name="user_lastname" id="user_lastname" class="input" value="{$last_name}" size="20"></label>
	</p>
<p>
		<label for="user_phone">Phone Number<br>
		<input type="text" name="user_phone" id="user_phone" class="input" value="{$phone}" size="20"></label>
	</p>
	<p>
		By clicking the `Register` button, you <strong>AGREE</strong> with our <a href="{$tac}" target="_blank">Terms and Conditions</a>.
	</p>
FORM;
	}
	
	function set_sponsor() {
		if( isset($_GET['action']) && $_GET['action'] == 'register') {
			if( isset($_GET['sponsor']) && $_GET['sponsor'] != '') {
				$user = get_userdata( $_GET['sponsor'] );
				if( $user !== false ) {
					$sponsor_id = $user->ID;
					setcookie('indavao_sponsor_id', $sponsor_id, (time()+60*60*24*30));
				}
			}
		}
	}
	
	function show_sponsor() {
		
		if( isset($_GET['action']) && $_GET['action'] == 'register') {
			$sponsor_id = 1;
			if( isset($_GET['sponsor']) && $_GET['sponsor'] != '') {
				$sponsor_id = $_GET['sponsor'];
			} else {
				if( isset($_COOKIE['indavao_sponsor_id']) && $_COOKIE['indavao_sponsor_id'] !== '') {
					$sponsor_id = $_COOKIE['indavao_sponsor_id'];
				} else {
					$sponsor_id = (get_option( '_indavao_default_sponsor_id') != '') ? get_option( '_indavao_default_sponsor_id') : 1;
				}
			}
			$user = get_userdata( $sponsor_id );
			if( $user !== false ) {
				return '<p class="message"><strong>Your Sponsor: </strong><a href="'.site_url('author/'. $user->user_login).'" target="_blank">'.$user->display_name.'</a><br></p>';
			}
		}
	}
	
	function registration_errors(  $errors, $sanitized_user_login, $user_email ) {
		if ( empty( $_POST['user_firstname'] ) || ! empty( $_POST['user_firstname'] ) && trim( $_POST['user_firstname'] ) == '' ) {
            $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'indavao' ) );
        }
        if ( empty( $_POST['user_lastname'] ) || ! empty( $_POST['user_lastname'] ) && trim( $_POST['user_lastname'] ) == '' ) {
            $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a last name.', 'indavao' ) );
        }
        if ( empty( $_POST['user_phone'] ) || ! empty( $_POST['user_phone'] ) && trim( $_POST['user_phone'] ) == '' ) {
            $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a phone number.', 'indavao' ) );
        }
		return $errors;
	}
	
	function user_register( $user_id ) {
		if ( ! empty( $_POST['user_firstname'] ) ) {
            update_user_meta( $user_id, 'first_name', trim( $_POST['user_firstname'] ) );
        }
        if ( ! empty( $_POST['user_lastname'] ) ) {
            update_user_meta( $user_id, 'last_name', trim( $_POST['user_lastname'] ) );
        }
        if ( ! empty( $_POST['user_phone'] ) ) {
            update_user_meta( $user_id, 'phone_number', trim( $_POST['user_phone'] ) );
        }
        if ( ! empty( $_POST['sponsor'] ) ) {
            update_user_meta( $user_id, 'sponsor_id', trim( $_POST['sponsor'] ) );
        }
	}
	
	function user_meta() {
		if( isset($_GET['user_id']) && $_GET['user_id'] != '') {
			$sponsor_id = get_user_meta( $_GET['user_id'], 'sponsor_id', true );
			if( $sponsor_id ) {
				$sponsor = get_userdata( $sponsor_id );
				$sponsr1 = new Custom_User_Meta(false, true);
				$sponsr1->set_label('Sponsor Info');
				$sponsr1->add_field(array('label' => "Sponsor Name", 'html'=>$sponsor->data->display_name, 'type' => 'html'));
				$sponsr1->init();
			}
		} else {
			global $current_user;
			$sponsor_id = get_user_meta( $current_user->ID, 'sponsor_id', true );
			if( $sponsor_id ) {
				$sponsor = get_userdata( $sponsor_id );
				$sponsr1 = new Custom_User_Meta(true, false);
				$sponsr1->set_label('Sponsor Info');
				$sponsr1->add_field(array('label' => "Sponsor Name", 'html'=>$sponsor->data->display_name, 'type' => 'html'));
				$sponsr1->init();
			}
		}
	}
}
