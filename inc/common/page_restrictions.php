<?php

function indavao_page_restrictions() {
	
	if( !current_user_can('manage_options') ) {
		$screen = get_current_screen();
		
		if( $screen->base == 'dashboard' ) {
			wp_redirect( 'profile.php' ); 
			exit;
		}
		
		$restricts['post_type'] = array('post', 'page', 'establishment', 'realestate', 'job', 'car', 'motorcycle', 'event', 'envira');
		$restricts['base'] = array('upload', 'media', 'tools', 'edit-comments', 'toplevel_page_jetpack');
		foreach( $restricts as $key=>$values) {
			foreach( $values as $value ) {
				if( isset($screen->$key) && ($screen->$key == $value) ) {
					$restricted = true;
				}
			}
		}

		if( $restricted ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ), 403 );
		}
	}
}
add_action( 'current_screen', 'indavao_page_restrictions' );
