<?php
function indavao_modify_contact_methods($profile_fields) {
	
	$profile_fields['phone_number'] = 'Phone Number';
	$profile_fields['skype'] = 'Skype ID';
	
	return $profile_fields;
}
add_filter('user_contactmethods', 'indavao_modify_contact_methods');
