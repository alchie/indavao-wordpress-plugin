<?php

function indavao_remove_admin_bar( $wp_admin_bar ) 
{  
	$nodes = array(
		'wp-logo',
		'about',
		'wporg',
		'documentation',
		'support-forums',
		'feedback',
	);
	
	foreach($nodes as $node) {
		$wp_admin_bar->remove_node( $node );
	}
	

	
	if( ! current_user_can('manage_options') ) {
		$nodes = array(
			'comments',
			'new-content',
			'establishment_parent',
			'realestate_parent',
			'job_parent',
			'car_parent',
			'motorcycle_parent',
			'event_parent',
			'view-site',
			'dashboard',
			/*
			'new-post',
			'new-media',
			'new-page',
			'new-establishment',
			'new-realestate',
			'new-job',
			'new-car',
			'new-motorcycle',
			'new-event',
			*/
		);

		foreach($nodes as $node) {
			$wp_admin_bar->remove_node( $node );
		}
	}
	
}
add_action( 'admin_bar_menu', 'indavao_remove_admin_bar', 999 );

function indavao_remove_admin_memu( $menu )  {
	
	/* debug menu
	global $menu, $submenu;
	foreach( $menu as $men ) {
		if( strpos( $men[2], '.php' ) > 0 ) {
			$url = $men[2];
		} else {
			$url = "admin.php?page=".$men[2];
		}
		echo "<a href=\"".admin_url($url)."\">{$men[0]}</a><br>\n";
	}
	foreach( $submenu as $smen ) {
		foreach( $smen as $smn ) {
			if( strpos( $smn[2], '.php' ) > 0 ) {
			$url = $smn[2];
			} else {
				$url = "admin.php?page=".$smn[2];
			}
			echo "<a href=\"".admin_url($url)."\">{$smn[0]}</a><br>\n";
		}
	}
	*/
	
	if( ! current_user_can('manage_options') ) {
		remove_menu_page( 'index.php' );
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'upload.php' );
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'edit.php?post_type=envira' );
		remove_menu_page( 'separator1' );
		remove_menu_page( 'separator2' );
		remove_menu_page( 'separator-last' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'link-manager.php' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit.php?post_type=establishment' );
		remove_menu_page( 'edit.php?post_type=realestate' );
		remove_menu_page( 'edit.php?post_type=job' );
		remove_menu_page( 'edit.php?post_type=car' );
		remove_menu_page( 'edit.php?post_type=motorcycle' );
		remove_menu_page( 'edit.php?post_type=event' );
		remove_menu_page( 'jetpack' );
	}
}
add_action( 'admin_menu', 'indavao_remove_admin_memu', 999 );


