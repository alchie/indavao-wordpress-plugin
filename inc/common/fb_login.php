<?php

class InDavaoFBLogin {
	function __construct() {
		add_action('wp_loaded', array(&$this, 'profile_login'));
		add_action('admin_enqueue_scripts', array(&$this,'profile_scripts'));
		add_action( 'wp_ajax_indavao_fb_login', array(&$this,'login_callback') );
		//add_action( 'wp_ajax_nopriv_indavao_fb_callback', array(&$this,'login_callback'));
		add_action( 'wp_ajax_indavao_fb_disconnect', array(&$this,'fb_disconnect' ));
		//add_action( 'wp_ajax_nopriv_indavao_fb_callback', array(&$this,'indavao_fb_callback'));
		//add_action('register_form', array(&$this,'registration_fb_login'));
		add_action('login_enqueue_scripts', array(&$this,'registration_scripts'));
	}
	
	function enqueue_scripts($facebook_app_id, $js_callback) {
	
echo <<<SCRIPTS
<script>
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      FB.api('/me', function(response) {
		{$js_callback}
	  });
    } 
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '{$facebook_app_id}',
    cookie     : true,  
    xfbml      : true,  
    version    : 'v2.2'
  });

  FB.getLoginStatus(function(response) {
	//statusChangeCallback(response);
  });

  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
</script>		
SCRIPTS;

	}

	function login_html($label='') {
		return '<fb:login-button data-auto-logout-link="true" data-size="large" scope="public_profile,email,user_friends,publish_actions" onlogin="checkLoginState();">'.$label.'</fb:login-button>';
	}
	
	function profile_login() {
		if (class_exists( 'Custom_User_Meta' )) {
			
			$fb_user_id = get_user_meta(get_current_user_id(), '_indavao_fb_user_id', TRUE);
			$usermeta = new Custom_User_Meta(true, false);
			$usermeta->set_label('Facebook Credentials');
			
			if( $fb_user_id == '' ) {
				$usermeta->add_field(array('label' => 'Connect Facebook Account', 'meta_key'=> '', 'meta_value'=> '', 'desc'=>'', 'type' => 'html', 'html'=> $this->login_html()));
			} else {
				$usermeta->add_field(array(
				'label' => 'Connected Facebook ID', 
				'meta_key'=> '', 
				'meta_value'=> '', 
				'desc'=>'', 
				'type' => 'html', 
				'html'=> '<a href="'.admin_url('admin-ajax.php?action=indavao_fb_disconnect').'" class="button button-primary">Disconnect FB Account</a>' ));
			}
			
			$usermeta->init();
			
		}
	}
	
	function profile_scripts() {
		global $pagenow;
		$facebook_app_id = get_option('_indavao_facebook_app_id');
		$fb_user_id = get_user_meta(get_current_user_id(), '_indavao_fb_user_id', TRUE);
		$redirect_url = admin_url('admin-ajax.php?action=indavao_fb_login');
		if(($pagenow == 'profile.php') && ($fb_user_id == '')) {
			$js_callback = "(function($, response) {
			$.ajax({
				method: 'POST',
				url : '{$redirect_url}',
				data: { fb_user_id: response.id }
			}).done(function(resp) {
				location.reload();
			});
		})(jQuery, response);";
			$this->enqueue_scripts($facebook_app_id, $js_callback);
		}
	}

	function login_callback() {
		if( isset($_POST['fb_user_id']) && $_POST['fb_user_id'] != '') {
			update_user_meta( get_current_user_id(), '_indavao_fb_user_id', $_POST['fb_user_id'] );
		}
		//echo json_encode( $_POST );
		wp_die();
	}

	function fb_disconnect() {
		delete_user_meta( get_current_user_id(), '_indavao_fb_user_id');
		if( isset( $_SERVER['HTTP_REFERER'] ) ) {
			header('location: ' . $_SERVER['HTTP_REFERER']);
		} else {
			header('location: ' . admin_url('profile.php'));
		}
		exit;
		wp_die();
	}
	
	function registration_fb_login() {
		echo '<p style="margin-bottom:10px">' . $this->login_html('Register using Facebook') . '</p>';
	}
	
	function registration_scripts() {
		global $pagenow;
		$facebook_app_id = get_option('_indavao_facebook_app_id');

		$js_callback = "(function($, response) {
			console.log( response );
		})(jQuery, response);";
			$this->enqueue_scripts($facebook_app_id, $js_callback);
		
	}
	
}










