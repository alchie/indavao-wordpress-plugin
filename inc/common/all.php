<?php

require('admin_menu.php');
require('page_restrictions.php');
require('referrals_redirects.php');
require('profile_page.php');


function indavao_load_custom_wp_admin_style() {
	global $plugin_dir;
    wp_register_style( 'indavao_admin_css', plugins_url( $plugin_dir . '/css/admin.css' ), false, '1.0.0' );
    wp_enqueue_style( 'indavao_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'indavao_load_custom_wp_admin_style' );
add_action( 'wp_enqueue_scripts', 'indavao_load_custom_wp_admin_style' );


function indavao_add_user_body_class($classes) {
	$user = wp_get_current_user();
	if( isset($user->roles) ) {
		foreach( $user->roles as $role ) {
			$classes .= ' ' . $role;
		}
	}
	return $classes;
}
add_filter( 'admin_body_class', 'indavao_add_user_body_class' );

require('fb_login.php');
$fb_login = new InDavaoFBLogin();

require('login_register.php');
$registration = new InDavaoNetworkRegistration(); 
