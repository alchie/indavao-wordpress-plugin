<?php

class InDavaoManagementMLM extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_mlm';
	public $title = 'Multi-Level Marketing';
	public $menu_name = 'Multi-Level';
	public $admin_bar = false;
	public $admin_init = true;
	public $admin_footer = true;
	public $admin_enqueue_scripts = true;
	
	function admin_enqueue_scripts() {
		wp_enqueue_media();
	}
	
	function admin_page() {
		
		if( isset($_GET['matrix']) && $_GET['matrix'] != '' ) {
			$this->mlm_matrix();
		} else {
			$this->mlm_list();
		}
		
	} 
	
	function mlm_list() {
		$link = admin_url('admin.php?page=' . $this->id);
		$tables = get_option('_indavao_networking_mlm_tables');
		$nonce = wp_nonce_field( $this->id );
		$new_n = 1;
		if( is_array( $tables ) ) {
			$new_n = count($tables) + 1;
		}

echo <<<HTML
		<div class="wrap">
		<h2>Multi-Level Marketing
			<a href="{$link}&add_new={$new_n}" class="add-new-h2">Add New Table</a>
		</h2>
HTML;

if( $_GET['updated'] == 1 ) {
	$this->notification( "New Table Added!" );
}
if( $_GET['updated'] == 2 ) {
	$this->notification( "Changes Saved!" );
}
if( $_GET['updated'] == 3 ) {
	$this->notification( "Table Removed!" );
}

if( isset($_REQUEST['add_new']) && $_REQUEST['add_new'] != '') {

echo <<<HTML
<h3>Add New Table</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_save">
<table class="form-table">
	<tbody>
<tr>
	<th><label for="table_order">Table Order</label></th>
	<td><input name="table_order" id="table_order" class="regular-text" type="text" value="{$_REQUEST['add_new']}">
	</td>
</tr>
<tr>
	<th><label for="table_id">Table ID</label></th>
	<td><input name="table_id" id="table_id" class="regular-text" type="text" value="">
	<p class="description">Must contain [0-9a-zA-Z_] only; no spaces</p>
	</td>
</tr>
<tr>
	<th><label for="table_name">Table Name</label></th>
	<td><input name="table_name" id="table_name" class="regular-text" type="text" value="">
	</td>
</tr>
<tr>
	<th><label for="table_entry">Table Entry</label></th>
	<td><input name="table_entry" id="table_entry" class="regular-text" type="text" value="">
	</td>
</tr>
<tr>
	<th><label for="table_earnings">Table Earnings</label></th>
	<td><input name="table_earnings" id="table_earnings" class="regular-text" type="text" value="">
	</td>
</tr>
<tr>
	<th><label for="table_screenshot">Table Screenshot</label></th>
	<td><input name="table_screenshot" id="table_screenshot" class="regular-text" type="text" value="">
	<a href="javascript:void(0);" data-id="table_screenshot" class="open_gallery"><i class="wp-menu-image dashicons-before dashicons-admin-media"></i></a>
	</td>
</tr>
HTML;
if( $tables ) {
	
	$options = '';
	if( is_array( $tables ) ) {
		foreach( $tables as $tbl) {
			$options .= '<option value="'.$tbl['table_id'].'">'.$tbl['table_name'].'</option>';
		}
	}
	
echo <<<HTML
<tr>
	<th><label for="table_requirement">Table Requirement</label></th>
	<td><select name="table_requirement" id="table_requirement">
	<option value="">No Requirement</option>
	{$options}
	</select>
	</td>
</tr>
HTML;
}
echo <<<HTML
</tbody></table>
<p class="submit">
<input name="submit" id="submit" class="button button-primary" value="Submit" type="submit">
<a class="button" href="{$link}">Cancel</a>
</p>
</form>
HTML;

} elseif( isset($_REQUEST['edit']) && $_REQUEST['edit'] != '') {

$selected = $tables[$_REQUEST['edit']];

echo <<<HTML
<h3>Edit Table</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_edit">
<input name="key" type="hidden" value="{$_REQUEST['edit']}">
<table class="form-table">
	<tbody>
<tr>
	<th><label for="table_order">Table Order</label></th>
	<td><input name="table_order" id="table_order" class="regular-text" type="text" value="{$selected['table_order']}">
	</td>
</tr>
<tr>
	<th><label for="table_id">Table ID</label></th>
	<td><input name="table_id" id="table_id" class="regular-text" type="text" value="{$selected['table_id']}">
	<p class="description">Must contain [0-9a-zA-Z_] only; no spaces</p>
	</td>
</tr>
<tr>
	<th><label for="table_name">Table Name</label></th>
	<td><input name="table_name" id="table_name" class="regular-text" type="text" value="{$selected['table_name']}">
	</td>
</tr>
<tr>
	<th><label for="table_entry">Table Entry</label></th>
	<td><input name="table_entry" id="table_entry" class="regular-text" type="text" value="{$selected['table_entry']}">
	</td>
</tr>
<tr>
	<th><label for="table_earnings">Table Earnings</label></th>
	<td><input name="table_earnings" id="table_earnings" class="regular-text" type="text" value="{$selected['table_earnings']}">
	</td>
</tr>
<tr>
	<th><label for="table_screenshot">Table Screenshot</label></th>
	<td><input name="table_screenshot" id="table_screenshot" class="regular-text" type="text" value="{$selected['table_screenshot']}">
	<a href="javascript:void(0);" data-id="table_screenshot" class="open_gallery"><i class="wp-menu-image dashicons-before dashicons-admin-media"></i></a>
	</td>
</tr>
HTML;
if( $tables ) {
	
	$options = '';
	if( is_array( $tables ) ) {
	foreach( $tables as $tbl ) {
		if( $tbl['table_id'] == $selected['table_requirement'] ) {
			$options .= '<option value="'.$tbl['table_id'].'" SELECTED>'.$tbl['table_name'].'</option>';
		} else {
			$options .= '<option value="'.$tbl['table_id'].'">'.$tbl['table_name'].'</option>';
		}
	}
	}
	
echo <<<HTML
<tr>
	<th><label for="table_requirement">Table Requirement</label></th>
	<td><select name="table_requirement" id="table_requirement">
	<option value="">No Requirement</option>
	{$options}
	</select>
	</td>
</tr>
HTML;
}
echo <<<HTML
</tbody></table>
<p class="submit">
<input name="submit" id="submit" class="button button-primary" value="Save Changes" type="submit">
<a class="button" href="{$link}">Cancel</a>
</p>
</form>
HTML;

} else {

if( !$tables ) {
	echo "<p>No Tables Found!</p>";
	return;
}

echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column column-cb check-column" style="padding-left:10px;">#</th>
		<th scope="col"  class="manage-column" >ID</th>
		<th scope="col"  class="manage-column" style="">Name</th>
		<th scope="col"  class="manage-column" style="">Entry</th>
		<th scope="col"  class="manage-column" style="">Earnings</th>
		<th scope="col"  class="manage-column" style="">Requirement</th>
		<th scope="col"  class="manage-column" style="">Screenshot</th>
		<th scope="col"  class="manage-column" style="text-align:right">Actions</th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;


	if( is_array( $tables ) ) {
foreach( $tables as $key=>$table ) {
	$screenshot = ($table['table_screenshot']!='') ? '<a href="'.$table['table_screenshot'].'" target="_blank">Preview</a>' : '';
	$table_entry = number_format($table['table_entry']);
	$table_earnings = number_format($table['table_earnings']);
echo <<<HTML
<tr>
	<th>{$table['table_order']}</th>
	<td>{$table['table_id']}</td>
	<td>{$table['table_name']}</td>
	<td>&#x20B1 {$table_entry}</td>
	<td>&#x20B1 {$table_earnings}</td>
	<td>{$tables[$table['table_requirement']]['table_name']}</td>
	<td>{$screenshot}</td>
	<td align="right"><a href="{$link}&edit={$key}">Edit</a> &middot; <a href="{$link}&delete={$key}">Delete</a> &middot; <a href="{$link}&matrix={$key}">Show Matrix</a></td>
</tr>
HTML;
	}
}

echo <<<HTML
	</tbody>
</table>
HTML;

}

echo <<<HTML
		</div>
HTML;
	}
	
	function mlm_matrix() {
		$tables = get_option('_indavao_networking_mlm_tables');
		$selected = $tables[$_GET['matrix']];
		$link = admin_url('admin.php?page=' . $this->id);

echo <<<HTML
		<div class="wrap">
		<h2>MLM Matrix - {$selected['table_name']}
			<a href="{$link}" class="add-new-h2">Back</a>
		</h2>
HTML;

echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column column-cb check-column" style="padding-left:10px;">#</th>
		<th scope="col"  class="manage-column" style="text-align:center;width:100px">Entries</th>
		<th scope="col"  class="manage-column" style="text-align:center;width:200px">Gross Income</th>
		<th scope="col"  class="manage-column" style="text-align:center;width:100px">Exits</th>
		<th scope="col"  class="manage-column" style="text-align:center;width:200px">Release</th>
		<th scope="col"  class="manage-column" style="text-align:center;width:200px">Net Income After Release</th>
		<th scope="col"  class="manage-column" style="text-align:center;width:200px">Partner Sharing</th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;

$entries = 1;
$exits = NULL;
for( $i=1; $i <= 20; $i++ ) {
	$income = ($entries * $selected['table_entry'] );
	$income2 = number_format( $income );
	$release = ($exits * $selected['table_earnings'] );
	$release2 = number_format( $release );
	$net = $income - $release;
	$net2 = number_format( $net );
	$sharing = $net / 2;
	$sharing2 = number_format( $sharing );
echo <<<HTML
<tr>
	<th>{$i}</th>
	<td style="text-align:center;">{$entries}</td>
	<td style="text-align:center;">&#x20B1 {$income2}</td>
	<td style="text-align:center;">{$exits}</td>
	<td style="text-align:center;">&#x20B1 {$release2}</td>
	<td style="text-align:center;">&#x20B1 {$net2}</td>
	<td style="text-align:center;">&#x20B1 {$sharing2}</td>
</tr>
HTML;
$entries = $entries * 2;
	if( $i == 3 ) {
		$exits = 1;
	}
	if( $i >= 4 ) {
		$exits = $exits * 2;
	} 
}


echo <<<HTML
	</tbody>
</table>
HTML;

	}
		
	function admin_init() {
		$this->tables_user_meta();
		if( $this->is_this_page() ) {
			$this->save_new_table();
		}
	}
	
	function _order_table($tables) {
		$unsorted = array();
		foreach( $tables as $table ) {
			$unsorted[$table['table_order']] = $table;
		}
		sort($unsorted);
		$sorted = array();
		foreach( $unsorted as $table ) {
			$sorted[$table['table_id']] = $table;
		}
		return $sorted;
	}
	
	function save_new_table() {
		if( !isset( $_GET['page'] ) || $_GET['page'] != $this->id ) {
			return;
		}
		if( isset( $_GET['delete'] ) && $_GET['delete'] != '') {
			
			$tables = get_option('_indavao_networking_mlm_tables');
			unset($tables[$_GET['delete']]);
			update_option('_indavao_networking_mlm_tables', $tables);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=3"));
			exit;
		}
		
		if( isset( $_POST['action'] ) && $_POST['action'] == $this->id . '_save') {
			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
				
				if( !isset( $_POST['table_order'] ) || $_POST['table_order'] == '') {
					return;
				}
				if( !isset( $_POST['table_id'] ) || $_POST['table_id'] == '') {
					return;
				}
				if( !isset( $_POST['table_name'] ) || $_POST['table_name'] == '') {
					return;
				}
				
				$tables = get_option('_indavao_networking_mlm_tables');
				if( !is_array( $tables ) ) {
					$tables = array();
				}
				$tables[$_POST['table_id']] = array(
					'table_order' => $_POST['table_order'],
					'table_id' => $_POST['table_id'],
					'table_name' => $_POST['table_name'],
					'table_entry' => $_POST['table_entry'],
					'table_earnings' => $_POST['table_earnings'],
					'table_requirement' => $_POST['table_requirement'],
					'table_screenshot' => $_POST['table_screenshot'],
				);
				
				$tables = $this->_order_table($tables);
				
				update_option('_indavao_networking_mlm_tables', $tables);
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
				exit;
				
			}
		} elseif( isset( $_POST['action'] ) && $_POST['action'] == $this->id.'_edit') {
			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
				
				if( !isset( $_POST['table_order'] ) || $_POST['table_order'] == '') {
					return;
				}
				if( !isset( $_POST['table_id'] ) || $_POST['table_id'] == '') {
					return;
				}
				if( !isset( $_POST['table_name'] ) || $_POST['table_name'] == '') {
					return;
				}
				
				$tables = get_option('_indavao_networking_mlm_tables');
				$tables[$_POST['key']] = array(
					'table_order' => $_POST['table_order'],
					'table_id' => $_POST['table_id'],
					'table_name' => $_POST['table_name'],
					'table_entry' => $_POST['table_entry'],
					'table_earnings' => $_POST['table_earnings'],
					'table_requirement' => $_POST['table_requirement'],
					'table_screenshot' => $_POST['table_screenshot'],
				);
				
				$tables = $this->_order_table($tables);
				
				update_option('_indavao_networking_mlm_tables', $tables);
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=2"));
				exit;
			}
		}
		
	}
	
	function tables_user_meta() {
			if (class_exists( 'Custom_User_Meta' )) {
				
					$tables = get_option('_indavao_networking_mlm_tables');
					
					$joined = new Custom_User_Meta(false);
					$joined->set_label('Payment Details');
					// payment details
					$joined->add_field(array('label' => "Bank Name and Branch", 'meta_key'=> '_indavao_user_payment_bank', 'meta_value'=> '', 'desc'=>'Bank Name and Branch', 'type' => 'text'));
					$joined->add_field(array('label' => "Deposit Reference ID", 'meta_key'=> '_indavao_user_payment_ref_id', 'meta_value'=> '', 'desc'=>'Deposit Reference ID', 'type' => 'text'));
					$joined->add_field(array('label' => "Date and Time", 'meta_key'=> '_indavao_user_payment_datetime', 'meta_value'=> '', 'desc'=>'Date and Time Deposited', 'type' => 'text'));
					// verify payment
					$joined->add_field(array('label' => "Verify Payment", 'meta_key'=> '_indavao_user_payment_verified', 'meta_value'=> '', 'desc'=>'Payment Verified', 'type' => 'checkbox'));
					$joined->init();
					
					$payment_verified = get_user_meta($_REQUEST['user_id'], '_indavao_user_payment_verified', true);	
					if( $payment_verified ) {
					
						if( is_array( $tables ) ) {
							$usermeta = new Custom_User_Meta(false);
							$usermeta->set_label('Networking Tables');

							foreach( $tables as $key=>$table ) {
									$table_active = get_user_meta($_REQUEST['user_id'], '_indavao_user_mlm_table_'.$table['table_id'].'_active', true);	
									$req = $tables[$table['table_requirement']];
									$req_active = get_user_meta($_REQUEST['user_id'], '_indavao_user_mlm_table_'.$req['table_id'].'_active', true);
									$req_exit = get_user_meta($_REQUEST['user_id'], '_indavao_user_mlm_table_'.$req['table_id'].'_exit', true);

									if( ($req == NULL) || ($req_active && $req_exit) ) {
										
										// update level						
										if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $_POST['user_id'] ) ) {		
											if( isset( $_POST['_indavao_user_mlm_table_'.$table['table_id'].'_parent_id'] ) && $_POST['_indavao_user_mlm_table_'.$table['table_id'].'_parent_id'] != '' ) {
												$user_parent_level = get_user_meta($_POST['_indavao_user_mlm_table_'.$table['table_id'].'_parent_id'], '_indavao_user_mlm_table_'.$table['table_id'].'_level', true);
												$user_parent_level = ($user_parent_level) ? $user_parent_level : 0;
												update_user_meta($_POST['user_id'], '_indavao_user_mlm_table_'.$table['table_id'].'_level', ($user_parent_level+1));
											}
										}
																
										$usermeta->add_field(array('add_label' => false, 'type' => 'html', 'html' => '<strong>' . $table['table_name'] .  '</strong><hr>'));
										$usermeta->add_field(array('label' => 'Activate', 'meta_key'=> '_indavao_user_mlm_table_'.$table['table_id'].'_active', 'meta_value'=> '', 'desc'=>'Check to Activate', 'type' => 'checkbox'));
										$usermeta->add_field(array('label' => 'Parent ID', 'meta_key'=> '_indavao_user_mlm_table_'.$table['table_id'].'_parent_id', 'meta_value'=> '', 'desc'=>'User ID of its predecessor', 'type' => 'text'));
										$usermeta->add_field(array('label' => 'MLM Level', 'meta_key'=> '_indavao_user_mlm_table_'.$table['table_id'].'_level', 'type' => 'show-value'));
										
										if( $table_active ) {
											$usermeta->add_field(array('label' => 'Exit', 'meta_key'=> '_indavao_user_mlm_table_'.$table['table_id'].'_exit', 'meta_value'=> '', 'desc'=>'Check to Exit', 'type' => 'checkbox'));
											$usermeta->add_field(array('label' => 'Check Number', 'meta_key'=> '_indavao_user_mlm_table_'.$table['table_id'].'_checknumber', 'meta_value'=> '', 'desc'=>'Check Number Released to User', 'type' => 'text'));
											$usermeta->add_field(array('label' => 'Date Issued', 'meta_key'=> '_indavao_user_mlm_table_'.$table['table_id'].'_checkdate', 'meta_value'=> '', 'desc'=>'Check Date Issued', 'type' => 'text'));
										}
									}
									
							}
						
							$usermeta->init();
						}
					
					}
				
			}
	}
	
	function admin_footer() {
		if( $this->not_this_page() ) {
			return;
		}
echo <<<JS
<script type="text/javascript">
(function($){

  // Set all variables to be used in scope
  var frame;
  
  // ADD IMAGE LINK
  $('.open_gallery').on( 'click', function( event ){
    var container_id = $(this).attr('data-id');

    event.preventDefault();
    
    // If the media frame already exists, reopen it.
    if ( frame ) {
      frame.open();
      return;
    }
    
    // Create a new media frame
    frame = wp.media({
      title: 'Select or Upload Media Of Your Chosen Persuasion',
      button: {
        text: 'Use this media'
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });

    
    // When an image is selected in the media frame...
    frame.on( 'select', function() {
      
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      // Send the attachment id to our hidden input
      $('#'+container_id).val( attachment.url );

    });

    // Finally, open the modal on click
    frame.open();
  });

})(jQuery);
</script>
JS;
	}
	
}
