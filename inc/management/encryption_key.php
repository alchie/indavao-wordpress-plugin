<?php

class InDavaoManagementEncryptionKey extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_encryption_key';
	public $title = 'Encryption Key';
	public $menu_name = 'Encryption Key';
	public $admin_bar = false;
	public $admin_init = true;
	
	function admin_page() {
		
		$nonce = wp_nonce_field( $this->id );
		$encryption_key = get_option( '_indavao_encryption_key');
echo <<<PHP
			<div class="wrap">
				<h2>{$this->title}</h2>
PHP;

if( $_GET['updated'] == 1 ) {
	$this->notification( "Changes Saved!" );
}

echo <<<PHP
		<form method="post">
			{$nonce}
			<input name="action" type="hidden" value="{$this->id}_save">
		<table class="form-table">
		<tbody><tr>
		<th scope="row"><label for="_indavao_encryption_key">{$this->title}</label></th>
		<td><input name="_indavao_encryption_key" type="text" id="_indavao_encryption_key" value="{$encryption_key}" class="regular-text"></td>
		</tr>
			</tbody></table>
			
		<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
		</form>
			</div>
PHP;

	}
	
	function admin_init() {
		
		if( $this->not_this_page() ) {	
			return;
		}
		
		if( !isset( $_POST['action'] ) || $_POST['action'] != $this->id . '_save') {
			return;
		}

		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
			update_option( '_indavao_encryption_key', $_POST['_indavao_encryption_key'] );
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
			exit;
		}
	}
}
