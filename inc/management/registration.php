<?php

class InDavaoManagementRegistration extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_registration';
	public $title = 'Registration';
	public $menu_name = 'Registration';
	public $admin_bar = false;
	public $admin_init = true;
	
	function admin_page() {
		$nonce = wp_nonce_field( $this->id );
		$default_sponsor_id = (get_option( '_indavao_default_sponsor_id') != '') ? get_option( '_indavao_default_sponsor_id') : 1;
		$tac = get_option( '_indavao_terms_and_conditions');
echo <<<PHP
			<div class="wrap">
				<h2>{$this->title}</h2>
PHP;

if( $_GET['updated'] == 1 ) {
	$this->notification( "Changes Saved!" );
}

echo <<<PHP
		<form method="post">
			{$nonce}
			<input name="action" type="hidden" value="{$this->id}_save">
		<table class="form-table">
		<tbody>
		<tr>
			<th scope="row"><label for="_indavao_default_sponsor_id">Default Sponsor ID</label></th>
			<td><input name="_indavao_default_sponsor_id" type="text" id="_indavao_default_sponsor_id" value="{$default_sponsor_id}" class="regular-text"></td>
		</tr>
		<tr>
			<th scope="row"><label for="_indavao_terms_and_conditions">Terms and Conditions Page URL</label></th>
			<td><input name="_indavao_terms_and_conditions" type="text" id="_indavao_terms_and_conditions" value="{$tac}" class="regular-text"></td>
		</tr>
			</tbody></table>
			
		<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
		</form>
			</div>
PHP;

	}
	
	function admin_init() {
		if( $this->is_this_page() ) {		
			if( !isset( $_POST['action'] ) || $_POST['action'] != $this->id . '_save') {
				return;
			}

			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
				update_option( '_indavao_default_sponsor_id', $_POST['_indavao_default_sponsor_id'] );
				update_option( '_indavao_terms_and_conditions', $_POST['_indavao_terms_and_conditions'] );
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
				exit;
			}
		}
	}
}
