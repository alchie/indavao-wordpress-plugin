<?php

class InDavaoManagementFacebookGroups extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_fb_groups';
	public $title = 'Facebook Groups';
	public $menu_name = 'Facebook Groups';
	public $admin_init = true;
		
	function admin_page() {
		$link = admin_url('admin.php?page=' . $this->id);
		$nonce = wp_nonce_field( $this->id );
		
echo <<<HTML
		<div class="wrap">
		<h2>Facebook Groups Collections
			<a href="{$link}&add_new=1" class="add-new-h2">Add New Collection</a>
		</h2>
		
HTML;

if( $_GET['updated'] == 1 ) {
	$this->notification( "New Collection Added!" );
}
if( $_GET['updated'] == 2 ) {
	$this->notification( "Changes Saved!" );
}
if( $_GET['updated'] == 3 ) {
	$this->notification( "Collection Removed!" );
}

if( isset($_REQUEST['add_new']) && $_REQUEST['add_new'] == 1) {
	
echo <<<HTML
<h3>New Facebook Groups Collection</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_save">
<table class="form-table">
	<tbody>
<tr class="user-first-name-wrap">
	<th><label for="collection_id">Collection ID</label></th>
	<td><input name="collection_id" id="collection_id" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="collection_name">Collection Name</label></th>
	<td><input name="collection_name" id="collection_name" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="collection_count">Collection Count</label></th>
	<td><input name="collection_count" id="collection_count" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="collection_url">Download Link</label></th>
	<td><input name="collection_url" id="collection_url" class="regular-text" type="text" value=""></td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="Submit" type="submit">
<a class="button" href="{$link}">Cancel</a></p>

</form>
HTML;

} elseif( isset($_REQUEST['edit']) && $_REQUEST['edit'] !== '') {
	$collections = get_option("_{$this->id}_data");
	$selected = $collections[$_REQUEST['edit']];
	
echo <<<HTML
<h3>Edit Team</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_edit">
<table class="form-table">
	<tbody>
<tr class="user-first-name-wrap">
	<th><label for="collection_id">Team ID</label></th>
	<td><input name="collection_id" id="collection_id" class="regular-text" type="text" value="{$selected['collection_id']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="collection_name">Team Name</label></th>
	<td><input name="collection_name" id="collection_name" class="regular-text" type="text" value="{$selected['collection_name']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="collection_count">Team Manager ID</label></th>
	<td><input name="collection_count" id="collection_count" class="regular-text" type="text" value="{$selected['collection_count']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="collection_url">Team Logo</label></th>
	<td><input name="collection_url" id="collection_url" class="regular-text" type="text" value="{$selected['collection_url']}">
	</td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="Save Changes" type="submit">
<a class="button" href="{$link}">Cancel</a></p>

</form>
HTML;


} else {
	
$collections = get_option("_{$this->id}_data");
if( !$collections ) {
	echo "<p>No Collection Found!</p>";
	return;
}

echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column" style="padding-left:10px">ID</th>
		<th scope="col"  class="manage-column" style="">Name</th>
		<th scope="col"  class="manage-column" style="">Count</th>
		<th scope="col"  class="manage-column" style="">Download Link</th>
		<th scope="col"  class="manage-column" style="text-align:right">Action</th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;

	if( is_array( $collections ) ) {
foreach( $collections as $key=>$collection ) {
	
echo <<<HTML
<tr>
	<th>{$collection['collection_id']}</th>
	<td>{$collection['collection_name']}</td>
	<td>{$collection['collection_count']}</td>
	<td>{$collection['collection_url']}</td>
	<td align="right"><a href="{$link}&edit={$key}">Edit</a> &middot; <a href="{$link}&delete={$key}">Delete</a></td>
</tr>
HTML;
	}
}

echo <<<HTML
	</tbody>
</table>
HTML;


HTML;
}

echo <<<HTML
</div>
HTML;
	}
	
	function admin_init() {
		if( $this->is_this_page() ) {
			$this->save_new_team();
		}
	}
	
	function save_new_team() {
		if( isset( $_GET['delete'] ) && $_GET['delete'] != '') {
			$collections = get_option("_{$this->id}_data");
			unset($collections[$_GET['delete']]);
			update_option("_{$this->id}_data", $collections);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=3"));
			exit;
		}
		if( isset( $_POST['action'] ) && $_POST['action'] == "{$this->id}_save") {
			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {

				$collections = get_option("_{$this->id}_data");
				
				if( !is_array( $collections ) ) {
					$collections = array();
				}
				$collections[$_POST['collection_id']] = array(
					'collection_id' => $_POST['collection_id'],
					'collection_name' => $_POST['collection_name'],
					'collection_count' => $_POST['collection_count'],
					'collection_url' => $_POST['collection_url'],
				);
				sort($collections);
				update_option("_{$this->id}_data", $collections);
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
				exit;
				
			}
		} elseif( isset( $_POST['action'] ) && $_POST['action'] == "{$this->id}_edit") {
			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
				$collections = get_option("_{$this->id}_data");
				if( !is_array( $collections ) ) {
					$collections = array();
				}
				$collections[$_POST['collection_id']] = array(
					'collection_id' => $_POST['collection_id'],
					'collection_name' => $_POST['collection_name'],
					'collection_count' => $_POST['collection_count'],
					'collection_url' => $_POST['collection_url'],
				);
				sort($collections);
				update_option("_{$this->id}_data", $collections);
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=2"));
				exit;
			}
		}
		
	}
	
	
		
}
