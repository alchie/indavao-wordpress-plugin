<?php

class InDavaoManagementFacebookOptions extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_facebook_options';
	public $title = 'Facebook Options';
	public $menu_name = 'Facebook Options';
	public $admin_bar = false;
	public $admin_init = true;
	
	function admin_page() {
		
		$nonce = wp_nonce_field( $this->id );
		$facebook_app_id = get_option( '_indavao_facebook_app_id');
echo <<<PHP
			<div class="wrap">
				<h2>Facebook Options</h2>
PHP;

if( $_GET['updated'] == 1 ) {
	$this->notification( "Changes Saved!" );
}

echo <<<PHP
		<form method="post">
			{$nonce}
			<input name="action" type="hidden" value="{$this->id}_save">
		<table class="form-table">
		<tbody><tr>
		<th scope="row"><label for="_indavao_facebook_app_id">Facebook App ID</label></th>
		<td><input name="_indavao_facebook_app_id" type="text" id="_indavao_facebook_app_id" value="{$facebook_app_id}" class="regular-text"></td>
		</tr>
			</tbody></table>
			
		<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
		</form>
			</div>
PHP;

	}
	
	function admin_init() {
		
		if( $this->not_this_page() ) {	
			return;
		}
		
		if( !isset( $_POST['action'] ) || $_POST['action'] != $this->id . '_save') {
			return;
		}

		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
			update_option( '_indavao_facebook_app_id', $_POST['_indavao_facebook_app_id'] );
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
			exit;
		}
	}
}
