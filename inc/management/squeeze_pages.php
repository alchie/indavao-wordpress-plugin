<?php

class InDavaoManagementSqueezePages extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_squeeze_pages';
	public $title = 'Squeeze Pages';
	public $menu_name = 'Squeeze Pages';
	public $admin_bar = false;
	public $admin_init = true;
	public $admin_footer = true;
	public $admin_enqueue_scripts = true;
	
	function admin_enqueue_scripts() {
		wp_enqueue_media();
	}
	
	function admin_page() {
		$link = admin_url('admin.php?page=' . $this->id);
		$nonce = wp_nonce_field( $this->id );
		
echo <<<HTML
		<div class="wrap">
		<h2>{$this->title}
			<a href="{$link}&add_new=1" class="add-new-h2">Add New Page</a>
		</h2>
		
HTML;

if( $_GET['updated'] == 1 ) {
	$this->notification( "New Page Added!" );
}
if( $_GET['updated'] == 2 ) {
	$this->notification( "Changes Saved!" );
}
if( $_GET['updated'] == 3 ) {
	$this->notification( "Page Removed!" );
}

if( isset($_REQUEST['add_new']) && $_REQUEST['add_new'] == 1) {
	
echo <<<HTML
<h3>Add New Page</h3>
<form method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_add">
<table class="form-table">
	<tbody>
<tr class="user-first-name-wrap">
	<th><label for="page_id">Page ID</label></th>
	<td><input name="page_id" id="page_id" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="page_name">Page Name</label></th>
	<td><input name="page_name" id="page_name" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="page_screenshot">Page Screenshot URL</label></th>
	<td><input name="page_screenshot" id="page_screenshot" class="regular-text" type="text" value="">
	<a href="javascript:void(0);" data-id="page_screenshot" class="open_gallery"><i class="wp-menu-image dashicons-before dashicons-admin-media"></i></a>
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="page_url">Page URL</label></th>
	<td><input name="page_url" id="page_url" class="regular-text" type="text" value="">
	</td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="Submit" type="submit">
<a class="button" href="{$link}">Cancel</a></p>

</form>
HTML;

} elseif( isset($_REQUEST['edit']) && $_REQUEST['edit'] !== '') {
	$pages = get_option('_indavao_squeeze_pages');
	$selected = $pages[$_REQUEST['edit']];
	
echo <<<HTML
<h3>Edit Team</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_edit">
<table class="form-table">
	<tbody>
<tr class="user-first-name-wrap">
	<th><label for="page_id">Page ID</label></th>
	<td><input name="page_id" id="page_id" class="regular-text" type="text" value="{$selected['page_id']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="page_name">Page Name</label></th>
	<td><input name="page_name" id="page_name" class="regular-text" type="text" value="{$selected['page_name']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="page_screenshot">Page Screenshot URL</label></th>
	<td><input name="page_screenshot" id="page_screenshot" class="regular-text" type="text" value="{$selected['page_screenshot']}">
	<a href="javascript:void(0);" data-id="page_screenshot" class="open_gallery"><i class="wp-menu-image dashicons-before dashicons-admin-media"></i></a>
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="page_url">Page URL</label></th>
	<td><input name="page_url" id="page_url" class="regular-text" type="text" value="{$selected['page_url']}">
	</td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="Save Changes" type="submit">
<a class="button" href="{$link}">Cancel</a></p>

</form>
HTML;


} else {

$pages = get_option('_indavao_squeeze_pages');

if( !$pages ) {
	echo "<p>No Tables Found!</p>";
	return;
}

echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column" style="padding-left:10px">Page ID</th>
		<th scope="col"  class="manage-column" style="">Page Name</th>
		<th scope="col"  class="manage-column" style="">Page URL</th>
		<th scope="col"  class="manage-column" style="">Screenshot</th>
		<th scope="col"  class="manage-column" style="text-align:right">Actions</th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;


	if( is_array( $pages ) ) {
foreach( $pages as $key=>$page ) {
	
	$page_url = ($page['page_screenshot']!='') ? '<a href="'.$page['page_url'].'" target="_blank">'.$page['page_url'].'</a>' : '';
	$screenshot = ($page['page_screenshot']!='') ? '<a href="'.$page['page_screenshot'].'" target="_blank">Preview Screenshot</a>' : '';
	
echo <<<HTML
<tr>
	<th>{$page['page_id']}</th>
	<td>{$page['page_name']}</td>
	<td>{$page_url}</td>
	<td>{$screenshot}</td>
	<td align="right"><a href="{$link}&edit={$key}">Edit</a> &middot; <a href="{$link}&delete={$key}">Delete</a></td>
</tr>
HTML;
	}
}

echo <<<HTML
	</tbody>
</table>
HTML;

}

echo <<<HTML
</div>
HTML;
	}
	
	function admin_init() {
		if( !isset( $_GET['page'] ) || $_GET['page'] != $this->id ) {
			return;
		}
		if( isset( $_GET['delete'] ) && $_GET['delete'] != '') {
			$pages = get_option('_indavao_squeeze_pages');
			unset($pages[$_GET['delete']]);
			update_option('_indavao_squeeze_pages', $pages);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=3"));
			exit;
		}
		
		if( ! isset( $_POST['action'] ) || ($_POST['action'] ==  '' )) {
			return;
		}
		
		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) && ($_POST['action'] == $this->id . '_add') ) {
				
				if( ! isset( $_POST['page_id'] ) || ($_POST['page_id'] ==  '' )) {
					return;
				}
				if( ! isset( $_POST['page_name'] ) || ($_POST['page_name'] ==  '' )) {
					return;
				}
				
				$pages = get_option('_indavao_squeeze_pages');
				if( !is_array( $pages ) ) {
					$pages = array();
				}
				$pages[$_POST['page_id']] = array(
					'page_id' => $_POST['page_id'],
					'page_name' => $_POST['page_name'],
					'page_screenshot' => $_POST['page_screenshot'],
					'page_url' => $_POST['page_url'],
				);
			
			update_option('_indavao_squeeze_pages', $pages);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
			exit;
		}
		
		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) && ($_POST['action'] == $this->id . '_edit') ) {
				
				if( ! isset( $_POST['page_id'] ) || ($_POST['page_id'] ==  '' )) {
					return;
				}
				if( ! isset( $_POST['page_name'] ) || ($_POST['page_name'] ==  '' )) {
					return;
				}
				
				$pages = get_option('_indavao_squeeze_pages');
				if( !is_array( $pages ) ) {
					$pages = array();
				}
				$pages[$_POST['page_id']] = array(
					'page_id' => $_POST['page_id'],
					'page_name' => $_POST['page_name'],
					'page_screenshot' => $_POST['page_screenshot'],
					'page_url' => $_POST['page_url'],
				);
			
			update_option('_indavao_squeeze_pages', $pages);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=2"));
			exit;
		}
	}
	
	function admin_footer() {
		if( $this->not_this_page() ) {
			return;
		}
echo <<<JS
<script type="text/javascript">
(function($){

  // Set all variables to be used in scope
  var frame;
  
  // ADD IMAGE LINK
  $('.open_gallery').on( 'click', function( event ){
    var container_id = $(this).attr('data-id');

    event.preventDefault();
    
    // If the media frame already exists, reopen it.
    if ( frame ) {
      frame.open();
      return;
    }
    
    // Create a new media frame
    frame = wp.media({
      title: 'Select or Upload Media Of Your Chosen Persuasion',
      button: {
        text: 'Use this media'
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });

    
    // When an image is selected in the media frame...
    frame.on( 'select', function() {
      
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      // Send the attachment id to our hidden input
      $('#'+container_id).val( attachment.url );

    });

    // Finally, open the modal on click
    frame.open();
  });

})(jQuery);
</script>
JS;
	}
}
