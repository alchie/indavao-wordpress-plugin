<?php

class InDavaoManagement extends Custom_Admin_Page {
	
	public $id = 'indavao_management';
	public $title = 'Management';
	public $menu_name = 'Management';
	public $icon = 'management.png';
	public $priority = '900';


function admin_page() {
		$link = admin_url('admin.php?page=' . $this->id);
echo <<<HTML
		<div class="wrap">
		<h2>{$this->title}</h2>
HTML;

$management = array(
	array(
		'id' => 'indavao_manage_squeeze_pages',
		'name' => 'Squeeze Pages',
		'url' => admin_url('admin.php?page=indavao_manage_squeeze_pages'),
	),
	array(
		'id' => 'indavao_manage_facebook_options',
		'name' => 'Facebook Options',
		'url' => admin_url('admin.php?page=indavao_manage_facebook_options'),
	),
	array(
		'id' => 'indavao_manage_registration',
		'name' => 'Registration',
		'url' => admin_url('admin.php?page=indavao_manage_registration'),
	),
	array(
		'id' => 'indavao_manage_mlm',
		'name' => 'Multi-Level Marketing',
		'url' => admin_url('admin.php?page=indavao_manage_mlm'),
	),
	array(
		'id' => 'indavao_manage_mlm_teams',
		'name' => 'Multi-level Marketing Teams',
		'url' => admin_url('admin.php?page=indavao_manage_mlm_teams'),
	),
	array(
		'id' => 'indavao_manage_fb_groups',
		'name' => 'Facebook Groups Collections',
		'url' => admin_url('admin.php?page=indavao_manage_fb_groups'),
	),
	array(
		'id' => 'indavao_manage_encryption_key',
		'name' => 'Encryption Key',
		'url' => admin_url('admin.php?page=indavao_manage_encryption_key'),
	),
);

echo "<div class=\"group-collections\">";
foreach( $management as $mgmt ) {
	echo <<<MGMT
<a href="{$mgmt['url']}" class="group-item"><strong>{$mgmt['name']}</strong></a>
MGMT;
}
echo "</div>";

echo "</div>";

	}
	
}
