<?php

class InDavaoManagementMySettings extends Custom_Admin_Page {

		public $id = 'indavao_my_settings';
		public $title = 'My Settings';
		public $menu_name = 'My Settings';
		public $permission = 'read';
		public $icon = 'settings.png';
		public $priority = '455';
		public $admin_bar = true;
		public $admin_init = true;
function admin_page() {
		global $plugin_dir;
		$nonce = wp_nonce_field( $this->id );
		$facebook_app_id = get_user_meta(get_current_user_id(), '_indavao_facebook_app_id', true);
		$help_img = plugins_url($plugin_dir.'/images/help.png');
echo <<<PHP
			<div class="wrap">
				<h2>{$this->title}</h2>
PHP;

if( $_GET['updated'] == 1 ) {
	$this->notification( "Changes Saved!" );
}

echo <<<PHP
		<form method="post">
			{$nonce}
			<input name="action" type="hidden" value="{$this->id}_save">
		<table class="form-table">
		<tbody><tr>
		<th scope="row"><label for="_indavao_facebook_app_id">Facebook App ID</label></th>
		<td><input name="_indavao_facebook_app_id" type="text" id="_indavao_facebook_app_id" value="{$facebook_app_id}" class="regular-text">
		<a href="http://help.indavao.net/2015/08/get-your-facebook-app-id.html" target="_blank"><img src="{$help_img}"></a>
		</td>
		</tr>
			</tbody></table>
			
		<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
		</form>
			</div>
PHP;

	}
	
	function admin_init() {
		
		if( $this->not_this_page() ) {	
			return;
		}
		
		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
			update_user_meta(get_current_user_id(), '_indavao_facebook_app_id', $_POST['_indavao_facebook_app_id']);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
			exit;
		}
	}
	
	
}

