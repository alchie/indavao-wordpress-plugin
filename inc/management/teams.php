<?php

class InDavaoManagementTeams extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_mlm_teams';
	public $title = 'MLM Teams';
	public $menu_name = 'MLM Teams';
	public $admin_bar = false;
	public $admin_init = true;
	public $admin_footer = true;
	public $admin_enqueue_scripts = true;
	
	function admin_enqueue_scripts() {
		wp_enqueue_media();
	}
	
	function admin_page() {
		$link = admin_url('admin.php?page=' . $this->id);
		$nonce = wp_nonce_field( $this->id );
		
echo <<<HTML
		<div class="wrap">
		<h2>Multi-level Marketing Teams
			<a href="{$link}&add_new=1" class="add-new-h2">Add New Team</a>
		</h2>
		
HTML;

if( $_GET['updated'] == 1 ) {
	$this->notification( "New Team Added!" );
}
if( $_GET['updated'] == 2 ) {
	$this->notification( "Changes Saved!" );
}
if( $_GET['updated'] == 3 ) {
	$this->notification( "Team Removed!" );
}

if( isset($_REQUEST['add_new']) && $_REQUEST['add_new'] == 1) {
	
echo <<<HTML
<h3>Add New Team</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="indavao_networking_mlm_team_save">
<table class="form-table">
	<tbody>
<tr class="user-first-name-wrap">
	<th><label for="team_id">Team ID</label></th>
	<td><input name="team_id" id="team_id" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="team_name">Team Name</label></th>
	<td><input name="team_name" id="team_name" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="team_manager">Team Manager ID</label></th>
	<td><input name="team_manager" id="team_manager" class="regular-text" type="text" value="">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="team_logo">Team Logo</label></th>
	<td><input name="team_logo" id="team_logo" class="regular-text" type="text" value="">
	<a href="javascript:void(0);" data-id="team_logo" class="open_gallery"><i class="wp-menu-image dashicons-before dashicons-admin-media"></i></a>
	</td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="Submit" type="submit">
<a class="button" href="{$link}">Cancel</a></p>

</form>
HTML;

} elseif( isset($_REQUEST['edit']) && $_REQUEST['edit'] !== '') {
	$teams = get_option('_indavao_networking_mlm_teams');
	$selected = $teams[$_REQUEST['edit']];
	
echo <<<HTML
<h3>Edit Team</h3>
<form action="" method="post">
{$nonce}
<input name="action" type="hidden" value="indavao_networking_mlm_edit">
<table class="form-table">
	<tbody>
<tr class="user-first-name-wrap">
	<th><label for="team_id">Team ID</label></th>
	<td><input name="team_id" id="team_id" class="regular-text" type="text" value="{$selected['team_id']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="team_name">Team Name</label></th>
	<td><input name="team_name" id="team_name" class="regular-text" type="text" value="{$selected['team_name']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="team_manager">Team Manager ID</label></th>
	<td><input name="team_manager" id="team_manager" class="regular-text" type="text" value="{$selected['team_manager']}">
	</td>
</tr>
<tr class="user-first-name-wrap">
	<th><label for="team_logo">Team Logo</label></th>
	<td><input name="team_logo" id="team_logo" class="regular-text" type="text" value="{$selected['team_logo']}">
	<a href="javascript:void(0);" data-id="team_logo" class="open_gallery"><i class="wp-menu-image dashicons-before dashicons-admin-media"></i></a>
	</td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="Save Changes" type="submit">
<a class="button" href="{$link}">Cancel</a></p>

</form>
HTML;


} else {
$teams = get_option('_indavao_networking_mlm_teams');
if( !$teams ) {
	echo "<p>No Teams Found!</p>";
	return;
}

echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column" style="padding-left:10px">Team ID</th>
		<th scope="col"  class="manage-column" style="">Team Name</th>
		<th scope="col"  class="manage-column" style="">Manager</th>
		<th scope="col"  class="manage-column" style="">Logo</th>
		<th scope="col"  class="manage-column" style="text-align:right">Action</th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;

	if( is_array( $teams ) ) {
foreach( $teams as $key=>$team ) {
	$manager = get_userdata($team['team_manager']);
	$logo = ($team['team_logo']!='') ? '<a href="'.$team['team_logo'].'" target="_blank">Preview</a>' : '';
	
echo <<<HTML
<tr>
	<th>{$team['team_id']}</th>
	<td>{$team['team_name']}</td>
	<td>{$manager->display_name}</td>
	<td>{$logo}</td>
	<td align="right"><a href="{$link}&edit={$key}">Edit</a> &middot; <a href="{$link}&delete={$key}">Delete</a></td>
</tr>
HTML;
	}
}

echo <<<HTML
	</tbody>
</table>
HTML;


HTML;
}

echo <<<HTML
</div>
HTML;
	}
	
	function admin_init() {
		$this->user_meta();
		if( $this->is_this_page() ) {
			$this->save_new_team();
		}
	}
	
	function save_new_team() {
		if( isset( $_GET['delete'] ) && $_GET['delete'] != '') {
			$teams = get_option('_indavao_networking_mlm_teams');
			unset($teams[$_GET['delete']]);
			update_option('_indavao_networking_mlm_teams', $teams);
			header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=3"));
			exit;
		}
		if( isset( $_POST['action'] ) && $_POST['action'] == 'indavao_networking_mlm_team_save') {
			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {

				$teams = get_option('_indavao_networking_mlm_teams');
				
				if( !is_array( $teams ) ) {
					$teams = array();
				}
				$teams[$_POST['team_id']] = array(
					'team_id' => $_POST['team_id'],
					'team_name' => $_POST['team_name'],
					'team_manager' => $_POST['team_manager'],
					'team_logo' => $_POST['team_logo'],
				);

				update_option('_indavao_networking_mlm_teams', $teams);
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=1"));
				exit;
				
			}
		} elseif( isset( $_POST['action'] ) && $_POST['action'] == 'indavao_networking_mlm_edit') {
			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
				$teams = get_option('_indavao_networking_mlm_teams');
				if( !is_array( $teams ) ) {
					$teams = array();
				}
				$teams[$_POST['team_id']] = array(
					'team_id' => $_POST['team_id'],
					'team_name' => $_POST['team_name'],
					'team_manager' => $_POST['team_manager'],
					'team_logo' => $_POST['team_logo'],
				);
				update_option('_indavao_networking_mlm_teams', $teams);
				header("location: ". admin_url('admin.php?page=' . $this->id . "&updated=2"));
				exit;
			}
		}
		
	}
	
	function user_meta() {
		if (class_exists( 'Custom_User_Meta' )) {
			
			$teams = get_option('_indavao_networking_mlm_teams');
		
			if( is_array( $teams ) ) {
				$options = array();
				foreach( $teams as $team ) {
					$options[] = array(
						'value' => $team['team_id'], 
						'label' => $team['team_name']
					);
				}

				$team = new Custom_User_Meta(false);
				$team->set_label('MLM Team');
				
				$team->add_field(array(
				'label' => "User MLM Team", 
				'meta_key'=> '_indavao_user_mlm_team', 
				'meta_value'=> '', 
				'desc'=>'', 
				'type' => 'select',
				'options' => $options));
				
				if( user_can( $_REQUEST['user_id'], 'edit_posts') ) {
					$team->add_field(array(
					'label' => "Manage Team", 
					'meta_key'=> '_indavao_user_can_manage_team', 
					'meta_value'=> '', 
					'desc'=>'Check If User is a Leader to manage his/her Team', 
					'type' => 'checkbox'));
				}
				
				$team->init();
			}
				
		}
	}
	
		function admin_footer() {
		if( $this->not_this_page() ) {
			return;
		}
echo <<<JS
<script type="text/javascript">
(function($){

  // Set all variables to be used in scope
  var frame;
  
  // ADD IMAGE LINK
  $('.open_gallery').on( 'click', function( event ){
    var container_id = $(this).attr('data-id');

    event.preventDefault();
    
    // If the media frame already exists, reopen it.
    if ( frame ) {
      frame.open();
      return;
    }
    
    // Create a new media frame
    frame = wp.media({
      title: 'Select or Upload Media Of Your Chosen Persuasion',
      button: {
        text: 'Use this media'
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });

    
    // When an image is selected in the media frame...
    frame.on( 'select', function() {
      
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      // Send the attachment id to our hidden input
      $('#'+container_id).val( attachment.url );

    });

    // Finally, open the modal on click
    frame.open();
  });

})(jQuery);
</script>
JS;
	}
}
