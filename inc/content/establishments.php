<?php 

if ( !class_exists('InDavaoEstablishments') )
{
	class InDavaoEstablishments extends InDavaoMain {
		public function setupPostType() {
			$this->setId( 'establishment' );
			$postType = $this->getPostType();
			$postType->set_slug('view')
					->set_name('Establishment')
					->set_singular('Establishment')
					->set_plural('Establishments')
					->set_menu_name('Local Business')
					->set_position(500)
					//->add_support('editor')
					//->add_support('author')
					->add_support('page-attributes')
					->add_support('thumbnail')
					//->add_support('excerpt')
					->set_hierarchical(true)
					->set_label('add_new', 'Add New Establishment')
					->set_label('add_new_item', 'Add New Establishment')
					//->set_show_in_menu(false)
					//->show_ui(false)
					->set_capability_type('post')
					;
			return $postType;
		}
		
		public function setupTaxonomy() {
			
			$location = new Custom_Taxonomy( $this->getId() );
			$location->set_id('establishment-location')
					->set_name('Location')
					->set_plural('Locations')
					->set_menu_name('Locations')
					->set_slug('localbusiness-location')
					->init();
					
			$category = new Custom_Taxonomy( $this->getId() );
			$category->set_id('establishment-category')
					->set_name('Category')
					->set_plural('Categories')
					->set_menu_name('Categories')
					->set_slug('list')
					->init();
			
			$post_tag = new Custom_Taxonomy( $this->getId() );
			$post_tag->set_id('post_tag')
						->init();
		}
		
		public function setupMetaboxes() {
			if ( !class_exists('Custom_Metabox') ) {
				return;
			}
			$details_id = 'establishment_details';
			$details = new Custom_Metabox( $this->getId() , $details_id, 'Establishment Details', 'advanced', 'high');
			$details->add_field( array(
				'type' => 'desc',
				'desc' => '<h3 style="padding-bottom:0px">Address</h3><hr>',
			) );
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Street',
				'placeholder' => 'Street',
				'id' => '_' . $details_id . '_address',
				'styles' => 'width:98%',
			) );
			$cities = get_terms('establishment-location', 'hide_empty=0');
			$cities_option = array();
			foreach( $cities as $city ) {
				if( $city->parent != 0 ) {
					$cities_option[] = array('label'=> $city->name, 'value'=> $city->name);
				}
			}
			$details->add_field( array(
				'type' => 'select',
				'label' => 'City',
				'placeholder' => 'City',
				'id' => '_' . $details_id . '_address_city',
				'styles' => 'width:98%',
				'options' => $cities_option,
				'default'=>'Davao City',
			) );
			$regions = get_terms('establishment-location', 'hide_empty=0');
			$regions_option = array();
			foreach( $regions as $region ) {
				if( $region->parent == 0 ) {
					$regions_option[] = array('label'=> $region->name, 'value'=> $region->name);
				}
			}
			$details->add_field( array(
				'type' => 'select',
				'label' => 'Region',
				'placeholder' => 'Region',
				'id' => '_' . $details_id . '_address_region',
				'styles' => 'width:98%',
				'options' => $regions_option,
				'default' => 'Davao del Sur'
			) );
			
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Postal Code',
				'placeholder' => 'Postal Code',
				'id' => '_' . $details_id . '_address_postal',
				'styles' => 'width:98%',
				'default' => 8000,
			));
			$details->add_field( array(
				'type' => 'desc',
				'desc' => '<h3 style="padding-bottom:0px">Phone Numbers</h3><hr>',
			) );
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Telephone Numbers',
				'placeholder' => 'Telephone Numbers',
				'id' => '_' . $details_id . '_phone',
				'styles' => 'width:98%',
			) );
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Mobile Phone Numbers',
				'placeholder' => 'Mobile Phone Numbers',
				'id' => '_' . $details_id . '_mobile',
				'styles' => 'width:98%',
			) );
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Fax Numbers',
				'placeholder' => 'Fax Numbers',
				'id' => '_' . $details_id . '_fax',
				'styles' => 'width:98%',
			) );
			
			$details->add_field( array(
				'type' => 'desc',
				'desc' => '<h3 style="padding-bottom:0px">Email And Website</h3><hr>',
			) );
			$details->add_field( array(
				'type' => 'email',
				'label' => 'Email Address',
				'placeholder' => 'Email Address',
				'id' => '_' . $details_id . '_email',
				'styles' => 'width:98%',
			) );
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Website',
				'placeholder' => 'Website',
				'id' => '_' . $details_id . '_website',
				'styles' => 'width:98%',
				'desc' => 'Don\'t forget to add prefix : <strong>http://</strong>',
			) );
			
			$details->add_field( array(
				'type' => 'desc',
				'desc' => '<h3 style="padding-bottom:0px">GeoLocation</h3><hr>',
			));
			
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Map Geo Location',
				'placeholder' => 'Map Geo Location',
				'id' => '_' . $details_id . '_mapgeo',
				'styles' => 'width:89%',
				'desc' => '<strong>FORMAT:</strong> [latitude],[longtitude] - separated by comma (,)',
				'update-button' => 'Get Geo Location',
				'update-callback' => '$(\'#_'.$details_id . '_mapgeo_update'.'\').click(function(){ 
					
					var getGeo = function(title, address, callback, n){
						console.log( address );
						$.ajax({
							url : \'https://maps.googleapis.com/maps/api/geocode/json?address=\'+encodeURIComponent(address)+\'&key=AIzaSyDBLtVV2Y6edCYAYYwf0WZgDn0Y5V0RqDs\',
							success : function(data) {
								if( data.results.length === 0) {
									if( n > 0 ) {
										n--;
										getGeo(title, title + \', \' +address, callback, n);
									}
								} else {
									callback( data );
								}
							}
						});
					};
					
					var title = $("#title").val();
					var address = $("#_establishment_details_address").val() 
					+ \', \' + $("#_establishment_details_address_city").val()
					+ \', \' + $("#_establishment_details_address_region").val()
					+ \', Philippines, \' + $("#_establishment_details_address_postal").val();
					
					getGeo(title, address, function(data) {
						var lat = data.results[0].geometry.location.lat;
						var lng = data.results[0].geometry.location.lng;
						$("#_establishment_details_mapgeo").val(lat + \',\' + lng);
					}, 1);
					$(this).remove();
				});'
			));
			
			$details->add_field( array(
				'type' => 'desc',
				'desc' => '<h3 style="padding-bottom:0px">Flickr Photos</h3><hr>',
			) );
			
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Flickr Photoset ID',
				'placeholder' => 'Flickr Photoset ID',
				'id' => '_' . $details_id . '_flickr_photoset',
				'styles' => 'width:98%',
			));
			
			$details->add_field( array(
				'type' => 'text',
				'label' => 'Flickr User ID',
				'placeholder' => 'Flickr User ID',
				'id' => '_' . $details_id . '_flickr_userid',
				'styles' => 'width:98%',
			));
			
			$details->init();
			
			$advance_id = 'establishment_advance';
			$advance = new Custom_Metabox( $this->getId() , $advance_id, 'Advanced Details', 'advanced', 'high');
			
$itemscopes = array(
				array('label'=> 'LocalBusiness', 'value'=> 'LocalBusiness'),
				    array('label'=> '- - - AnimalShelter', 'value'=> 'AnimalShelter'), //AnimalShelter
                 
                 array('label'=> 'AutomotiveBusiness', 'value'=> 'AutomotiveBusiness'), //AutomotiveBusiness
                 
array('label'=> '- - - AutoBodyShop', 'value'=> 'AutoBodyShop'), //AutoBodyShop
array('label'=> '- - - AutoDealer', 'value'=> 'AutoDealer'), //AutoDealer
array('label'=> '- - - AutoPartsStore', 'value'=> 'AutoPartsStore'), //AutoPartsStore
array('label'=> '- - - AutoRental', 'value'=> 'AutoRental'), //AutoRental
array('label'=> '- - - AutoRepair', 'value'=> 'AutoRepair'), //AutoRepair
array('label'=> '- - - AutoWash', 'value'=> 'AutoWash'), //AutoWash
array('label'=> '- - - GasStation', 'value'=> 'GasStation'), //GasStation
array('label'=> '- - - MotorcycleDealer', 'value'=> 'MotorcycleDealer'), //MotorcycleDealer
array('label'=> '- - - MotorcycleRepair', 'value'=> 'MotorcycleRepair'), //MotorcycleRepair
            
                 
    array('label'=> '- - - ChildCare', 'value'=> 'ChildCare'), //ChildCare
                 
    array('label'=> '- - - DryCleaningOrLaundry', 'value'=> 'DryCleaningOrLaundry'), //DryCleaningOrLaundry
                 
                 array('label'=> 'EmergencyService', 'value'=> 'EmergencyService'), //EmergencyService
                 
array('label'=> '- - - FireStation', 'value'=> 'FireStation'), //FireStation
array('label'=> '- - - Hospital', 'value'=> 'Hospital'), //Hospital
array('label'=> '- - - PoliceStation', 'value'=> 'PoliceStation'), //PoliceStation
            
                 
    array('label'=> '- - - EmploymentAgency', 'value'=> 'EmploymentAgency'), //EmploymentAgency
                 
                 array('label'=> 'EntertainmentBusiness', 'value'=> 'EntertainmentBusiness'), //EntertainmentBusiness
                 
array('label'=> '- - - AdultEntertainment', 'value'=> 'AdultEntertainment'), //AdultEntertainment
array('label'=> '- - - AmusementPark', 'value'=> 'AmusementPark'), //AmusementPark
array('label'=> '- - - ArtGallery', 'value'=> 'ArtGallery'), //ArtGallery
array('label'=> '- - - Casino', 'value'=> 'Casino'), //Casino
array('label'=> '- - - ComedyClub', 'value'=> 'ComedyClub'), //ComedyClub
array('label'=> '- - - MovieTheater', 'value'=> 'MovieTheater'), //MovieTheater
array('label'=> '- - - NightClub', 'value'=> 'NightClub'), //NightClub
            
                 
                 array('label'=> 'FinancialService', 'value'=> 'FinancialService'), //FinancialService
                 
array('label'=> '- - - AccountingService', 'value'=> 'AccountingService'), //AccountingService
array('label'=> '- - - AutomatedTeller', 'value'=> 'AutomatedTeller'), //AutomatedTeller
array('label'=> '- - - BankOrCreditUnion', 'value'=> 'BankOrCreditUnion'), //BankOrCreditUnion
array('label'=> '- - - InsuranceAgency', 'value'=> 'InsuranceAgency'), //InsuranceAgency
            
                 
                 array('label'=> 'FoodEstablishment', 'value'=> 'FoodEstablishment'), //FoodEstablishment
                 
array('label'=> '- - - Bakery', 'value'=> 'Bakery'), //Bakery
array('label'=> '- - - BarOrPub', 'value'=> 'BarOrPub'), //BarOrPub
array('label'=> '- - - Brewery', 'value'=> 'Brewery'), //Brewery
array('label'=> '- - - CafeOrCoffeeShop', 'value'=> 'CafeOrCoffeeShop'), //CafeOrCoffeeShop
array('label'=> '- - - FastFoodRestaurant', 'value'=> 'FastFoodRestaurant'), //FastFoodRestaurant
array('label'=> '- - - IceCreamShop', 'value'=> 'IceCreamShop'), //IceCreamShop
array('label'=> '- - - Restaurant', 'value'=> 'Restaurant'), //Restaurant
array('label'=> '- - - Winery', 'value'=> 'Winery'), //Winery
            
                 
                 array('label'=> 'GovernmentOffice', 'value'=> 'GovernmentOffice'), //GovernmentOffice
                 
array('label'=> '- - - PostOffice', 'value'=> 'PostOffice'), //PostOffice
            
                 
                 array('label'=> 'HealthAndBeautyBusiness', 'value'=> 'HealthAndBeautyBusiness'), //HealthAndBeautyBusiness
                 
array('label'=> '- - - BeautySalon', 'value'=> 'BeautySalon'), //BeautySalon
array('label'=> '- - - DaySpa', 'value'=> 'DaySpa'), //DaySpa
array('label'=> '- - - HairSalon', 'value'=> 'HairSalon'), //HairSalon
array('label'=> '- - - HealthClub', 'value'=> 'HealthClub'), //HealthClub
array('label'=> '- - - NailSalon', 'value'=> 'NailSalon'), //NailSalon
array('label'=> '- - - TattooParlor', 'value'=> 'TattooParlor'), //TattooParlor
            
                 
                 array('label'=> 'HomeAndConstructionBusiness', 'value'=> 'HomeAndConstructionBusiness'), //HomeAndConstructionBusiness
                 
array('label'=> '- - - Electrician', 'value'=> 'Electrician'), //Electrician
array('label'=> '- - - GeneralContractor', 'value'=> 'GeneralContractor'), //GeneralContractor
array('label'=> '- - - HVACBusiness', 'value'=> 'HVACBusiness'), //HVACBusiness
array('label'=> '- - - HousePainter', 'value'=> 'HousePainter'), //HousePainter
array('label'=> '- - - Locksmith', 'value'=> 'Locksmith'), //Locksmith
array('label'=> '- - - MovingCompany', 'value'=> 'MovingCompany'), //MovingCompany
array('label'=> '- - - Plumber', 'value'=> 'Plumber'), //Plumber
array('label'=> '- - - RoofingContractor', 'value'=> 'RoofingContractor'), //RoofingContractor
            
                 
    array('label'=> '- - - InternetCafe', 'value'=> 'InternetCafe'), //InternetCafe
                 
    array('label'=> '- - - Library', 'value'=> 'Library'), //Library
                 
                 array('label'=> 'LodgingBusiness', 'value'=> 'LodgingBusiness'), //LodgingBusiness
                 
array('label'=> '- - - BedAndBreakfast', 'value'=> 'BedAndBreakfast'), //BedAndBreakfast
array('label'=> '- - - Hostel', 'value'=> 'Hostel'), //Hostel
array('label'=> '- - - Hotel', 'value'=> 'Hotel'), //Hotel
array('label'=> '- - - Motel', 'value'=> 'Motel'), //Motel
            
                 
                 array('label'=> 'MedicalOrganization', 'value'=> 'MedicalOrganization'), //MedicalOrganization
                 
array('label'=> '- - - Dentist', 'value'=> 'Dentist'), //Dentist
array('label'=> '- - - DiagnosticLab', 'value'=> 'DiagnosticLab'), //DiagnosticLab
array('label'=> '- - - Hospital', 'value'=> 'Hospital'), //Hospital
array('label'=> '- - - MedicalClinic', 'value'=> 'MedicalClinic'), //MedicalClinic
array('label'=> '- - - Optician', 'value'=> 'Optician'), //Optician
array('label'=> '- - - Pharmacy', 'value'=> 'Pharmacy'), //Pharmacy
array('label'=> '- - - Physician', 'value'=> 'Physician'), //Physician
array('label'=> '- - - VeterinaryCare', 'value'=> 'VeterinaryCare'), //VeterinaryCare
            
                 
                 array('label'=> 'ProfessionalService', 'value'=> 'ProfessionalService'), //ProfessionalService
                 
array('label'=> '- - - AccountingService', 'value'=> 'AccountingService'), //AccountingService
array('label'=> '- - - Attorney', 'value'=> 'Attorney'), //Attorney
array('label'=> '- - - Dentist', 'value'=> 'Dentist'), //Dentist
array('label'=> '- - - Electrician', 'value'=> 'Electrician'), //Electrician
array('label'=> '- - - GeneralContractor', 'value'=> 'GeneralContractor'), //GeneralContractor
array('label'=> '- - - HousePainter', 'value'=> 'HousePainter'), //HousePainter
array('label'=> '- - - Locksmith', 'value'=> 'Locksmith'), //Locksmith
array('label'=> '- - - Notary', 'value'=> 'Notary'), //Notary
array('label'=> '- - - Plumber', 'value'=> 'Plumber'), //Plumber
array('label'=> '- - - RoofingContractor', 'value'=> 'RoofingContractor'), //RoofingContractor
            
                 
    array('label'=> '- - - RadioStation', 'value'=> 'RadioStation'), //RadioStation
                 
    array('label'=> '- - - RealEstateAgent', 'value'=> 'RealEstateAgent'), //RealEstateAgent
                 
    array('label'=> '- - - RecyclingCenter', 'value'=> 'RecyclingCenter'), //RecyclingCenter
                 
    array('label'=> '- - - SelfStorage', 'value'=> 'SelfStorage'), //SelfStorage
                 
    array('label'=> '- - - ShoppingCenter', 'value'=> 'ShoppingCenter'), //ShoppingCenter
                 
array('label'=> 'SportsActivityLocation', 'value'=> 'SportsActivityLocation'), //SportsActivityLocation                 
array('label'=> '- - - BowlingAlley', 'value'=> 'BowlingAlley'), //BowlingAlley
array('label'=> '- - - ExerciseGym', 'value'=> 'ExerciseGym'), //ExerciseGym
array('label'=> '- - - GolfCourse', 'value'=> 'GolfCourse'), //GolfCourse
array('label'=> '- - - HealthClub', 'value'=> 'HealthClub'), //HealthClub
array('label'=> '- - - PublicSwimmingPool', 'value'=> 'PublicSwimmingPool'), //PublicSwimmingPool
array('label'=> '- - - SkiResort', 'value'=> 'SkiResort'), //SkiResort
array('label'=> '- - - SportsClub', 'value'=> 'SportsClub'), //SportsClub
array('label'=> '- - - StadiumOrArena', 'value'=> 'StadiumOrArena'), //StadiumOrArena
array('label'=> '- - - TennisComplex', 'value'=> 'TennisComplex'), //TennisComplex

array('label'=> 'Store', 'value'=> 'Store'), //Store             
array('label'=> '- - - AutoPartsStore', 'value'=> 'AutoPartsStore'), //AutoPartsStore
array('label'=> '- - - BikeStore', 'value'=> 'BikeStore'), //BikeStore
array('label'=> '- - - BookStore', 'value'=> 'BookStore'), //BookStore
array('label'=> '- - - ClothingStore', 'value'=> 'ClothingStore'), //ClothingStore
array('label'=> '- - - ComputerStore', 'value'=> 'ComputerStore'), //ComputerStore
array('label'=> '- - - ConvenienceStore', 'value'=> 'ConvenienceStore'), //ConvenienceStore
array('label'=> '- - - DepartmentStore', 'value'=> 'DepartmentStore'), //DepartmentStore
array('label'=> '- - - ElectronicsStore', 'value'=> 'ElectronicsStore'), //ElectronicsStore
array('label'=> '- - - Florist', 'value'=> 'Florist'), //Florist
array('label'=> '- - - FurnitureStore', 'value'=> 'FurnitureStore'), //FurnitureStore
array('label'=> '- - - GardenStore', 'value'=> 'GardenStore'), //GardenStore
array('label'=> '- - - GroceryStore', 'value'=> 'GroceryStore'), //GroceryStore
array('label'=> '- - - HardwareStore', 'value'=> 'HardwareStore'), //HardwareStore
array('label'=> '- - - HobbyShop', 'value'=> 'HobbyShop'), //HobbyShop
array('label'=> '- - - HomeGoodsStore', 'value'=> 'HomeGoodsStore'), //HomeGoodsStore
array('label'=> '- - - JewelryStore', 'value'=> 'JewelryStore'), //JewelryStore
array('label'=> '- - - LiquorStore', 'value'=> 'LiquorStore'), //LiquorStore
array('label'=> '- - - MensClothingStore', 'value'=> 'MensClothingStore'), //MensClothingStore
array('label'=> '- - - MobilePhoneStore', 'value'=> 'MobilePhoneStore'), //MobilePhoneStore
array('label'=> '- - - MovieRentalStore', 'value'=> 'MovieRentalStore'), //MovieRentalStore
array('label'=> '- - - MusicStore', 'value'=> 'MusicStore'), //MusicStore
array('label'=> '- - - OfficeEquipmentStore', 'value'=> 'OfficeEquipmentStore'), //OfficeEquipmentStore
array('label'=> '- - - OutletStore', 'value'=> 'OutletStore'), //OutletStore
array('label'=> '- - - PawnShop', 'value'=> 'PawnShop'), //PawnShop
array('label'=> '- - - PetStore', 'value'=> 'PetStore'), //PetStore
array('label'=> '- - - ShoeStore', 'value'=> 'ShoeStore'), //ShoeStore
array('label'=> '- - - SportingGoodsStore', 'value'=> 'SportingGoodsStore'), //SportingGoodsStore
array('label'=> '- - - TireShop', 'value'=> 'TireShop'), //TireShop
array('label'=> '- - - ToyStore', 'value'=> 'ToyStore'), //ToyStore
array('label'=> '- - - WholesaleStore', 'value'=> 'WholesaleStore'), //WholesaleStore
                    
    array('label'=> '- - - TelevisionStation', 'value'=> 'TelevisionStation'), //TelevisionStation        
    array('label'=> '- - - TouristInformationCenter', 'value'=> 'TouristInformationCenter'), //TouristInformationCenter  
    array('label'=> '- - - TravelAgency', 'value'=> 'TravelAgency'), //TravelAgency
                 

			);
			$advance->add_field( array(
				'type' => 'select',
				'label' => 'Schema ItemScope',
				'placeholder' => 'Schema ItemScope',
				'id' => '_' . $details_id . '_itemscope',
				'styles' => 'width:98%',
				'options' => $itemscopes,
				'default' => 'LocalBusiness',
			));
			
			$advance->init();
			
			$social_media_id = 'establishment_social_media';
			$social_media = new Custom_Metabox( $this->getId() , $social_media_id, 'Social Media Pages', 'advanced', 'high');
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Facebook Page URL',
				'placeholder' => 'Facebook Page URL',
				'id' => '_' . $social_media_id . '_facebook',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Google Plus URL',
				'placeholder' => 'Google Plus URL',
				'id' => '_' . $social_media_id . '_google',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Twitter URL',
				'placeholder' => 'Twitter URL',
				'id' => '_' . $social_media_id . '_twitter',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'LinkedIn Company URL',
				'placeholder' => 'LinkedIn Company URL',
				'id' => '_' . $social_media_id . '_linkedin',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Youtube Channel URL',
				'placeholder' => 'Youtube Channel URL',
				'id' => '_' . $social_media_id . '_youtube',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Vimeo URL',
				'placeholder' => 'Vimeo URL',
				'id' => '_' . $social_media_id . '_vimeo',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Blogger URL',
				'placeholder' => 'Blogger URL',
				'id' => '_' . $social_media_id . '_blogger',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Behance URL',
				'placeholder' => 'Behance URL',
				'id' => '_' . $social_media_id . '_behance',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Deviantart URL',
				'placeholder' => 'Deviantart URL',
				'id' => '_' . $social_media_id . '_deviantart',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Digg URL',
				'placeholder' => 'Digg URL',
				'id' => '_' . $social_media_id . '_digg',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Dribble URL',
				'placeholder' => 'Dribble URL',
				'id' => '_' . $social_media_id . '_dribble',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Pinterest URL',
				'placeholder' => 'Pinterest URL',
				'id' => '_' . $social_media_id . '_pinterest',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Stumbleupon URL',
				'placeholder' => 'Stumbleupon URL',
				'id' => '_' . $social_media_id . '_stumbleupon',
				'styles' => 'width:98%',
			));
			$social_media->add_field( array(
				'type' => 'text',
				'label' => 'Tumblr URL',
				'placeholder' => 'Tumblr URL',
				'id' => '_' . $social_media_id . '_tumblr',
				'styles' => 'width:98%',
			));
			$social_media->init();
			
		if( !current_user_can('contributor')  && !current_user_can('author')) {
				
			$advertising_id = 'establishment_advertising';
			$advertising = new Custom_Metabox( $this->getId() , $advertising_id, 'Advertising', 'side', 'high');
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Call Directly',
				'placeholder' => 'Call Directly',
				'id' => '_' . $advertising_id . '_call_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'text',
				'label' => 'Primary Phone',
				'placeholder' => 'Primary Phone',
				'id' => '_' . $advertising_id . '_phone_primary',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Email Directly',
				'placeholder' => 'Email Directly',
				'id' => '_' . $advertising_id . '_email_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'text',
				'label' => 'Primary Email',
				'placeholder' => 'Primary Email',
				'id' => '_' . $advertising_id . '_email_primary',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Share Button',
				'placeholder' => 'Share Button',
				'id' => '_' . $advertising_id . '_share_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Reviews Button',
				'placeholder' => 'Reviews Button',
				'id' => '_' . $advertising_id . '_reviews_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Map Button',
				'placeholder' => 'Map Button',
				'id' => '_' . $advertising_id . '_map_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Gallery Button',
				'placeholder' => 'Gallery Button',
				'id' => '_' . $advertising_id . '_gallery_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Favorite Button',
				'placeholder' => 'Favorite Button',
				'id' => '_' . $advertising_id . '_favorite_active',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Searchable',
				'placeholder' => 'Searchable',
				'id' => '_' . $advertising_id . '_searchable',
				'styles' => 'width:98%',
			));
			$advertising->add_field( array(
				'type' => 'checkbox',
				'label' => 'Verified Business',
				'placeholder' => 'Verified Business',
				'id' => '_' . $advertising_id . '_verified',
				'styles' => 'width:98%',
			));
			$advertising->init();
		}
			$logo_id = 'establishment_logo';
			$logo = new Custom_Metabox( $this->getId() , $logo_id, 'Logo', 'side', 'low');
			$logo->add_field( array(
				'type' => 'post-thumbnail',
				'label' => 'Set Logo',
				'id' => '_' . $logo_id . '_logo',
			));
			$logo->init();
			
		}
		
		public function rowActions( $actions, $post ) {
			$actions['view'] = str_replace('href', 'target="_blank" href', $actions['view']);
			return $actions;
		}
		
		public function adminBar() 
		{
			if( current_user_can('subscriber') ) {
				return;
			}
			
			global $wp_admin_bar;  
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_parent', 
				'title' => 'Local Business',
				'href' => admin_url() . 'edit.php?post_type=' . $this->getId(),
			)); 
			
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_add', 
				'title' => 'Add New',
				'href' => admin_url() . 'post-new.php?post_type=' . $this->getId(),
				'parent' => $this->getId() . '_parent',
			)); 
			
			
			if( !current_user_can('contributor')  && !current_user_can('author')) {
				$wp_admin_bar->add_node(array(
					'id' => $this->getId() . '_categories', 
					'title' => 'Categories',
					'href' => admin_url() . 'edit-tags.php?post_type=' . $this->getId() . '&taxonomy=establishment-category',
					'parent' => $this->getId() . '_parent',
				));
				
				$wp_admin_bar->add_node(array(
					'id' => $this->getId() . '_location', 
					'title' => 'Locations',
					'href' => admin_url() . 'edit-tags.php?post_type=' . $this->getId() . '&taxonomy=establishment-location',
					'parent' => $this->getId() . '_parent',
				));
				
				$wp_admin_bar->add_node(array(
					'id' => $this->getId() . '_post_tag', 
					'title' => 'Tags',
					'href' => admin_url() . 'edit-tags.php?post_type=' . $this->getId() . '&taxonomy=post_tag',
					'parent' => $this->getId() . '_parent',
				));
			}
			
		}
	}
}
