<?php 

if ( !class_exists('InDavaoMain') )
{
	class InDavaoMain {
        protected $post_type_id;
        protected $postType;
		
		public function __construct() {
			$this->postType = new Custom_Post_Type; 
		}
		
		public function setId( $id ) { 
			$this->post_type_id = $id;
		}
		
		public function getId() { 
			return $this->post_type_id;
		}
		
		public function getPostType() { 
			return $this->postType;
		}
				
        public function init() {
			
			$this->setupPostType();
			
			if( $this->post_type_id == '' ) {
				return;
			}
			
			$this->postType->set_id( $this->getId() )->init();
			$this->setupTaxonomy();
			
			add_action( 'current_screen', array($this,'setupMetaboxes') );
			add_filter('post_row_actions', array($this,'rowActions'), 10, 2);
			add_filter('page_row_actions', array($this,'rowActions'), 10, 2);
			add_action( 'admin_bar_menu', array($this,'adminBar'), 999 );
			/*
			 * add_action( 'current_screen', array($this,'checkRequirement') );
			add_action( 'all_admin_notices', array($this,'tabs') );
			add_action( "load-{$GLOBALS['pagenow']}", array($this,'help') );
			add_filter('manage_'.$this->post_type_id.'_posts_columns', array($this,'postsColumns'));
			
			add_filter('bulk_actions-edit-' . $this->post_type_id, array($this,'bulkActions'));
			add_action( 'save_post', array($this,'savePost') );
			add_filter( "views_edit-" . $this->post_type_id, array($this,'subSubSub') );
			add_filter( "redirect_post_location", array($this,'redirectPostLocation'), 10, 2 );
			add_action( 'pre_get_posts', array($this,'preGetPosts') ); 
			* */
        }
        
        public function setupPostType() {}
        
        public function setupTaxonomy() {}
        
        public function setupMetaboxes() {}
        
        public function checkRequirement() {}
		
		public function tabs() {}
		
		public function help() {}   
		
		public function postsColumns($defaults) { return $defaults; }
        
        public function rowActions( $actions, $post ) { return $actions; }
		
		public function bulkActions( $actions ) { return $actions; }
		
		public function savePost( $post_id ) {}
		
		public function subSubSub( $views ) { return $views; }
		
		public function redirectPostLocation( $location, $post_id ) { return $location; }
		
		public function preGetPosts( $query ) {}
		
		public function adminBar() {}
    }
}
