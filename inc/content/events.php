<?php 

if ( !class_exists('InDavaoEvents') )
{
	class InDavaoEvents extends InDavaoMain {
		public function setupPostType() {
			$this->setId( 'event' );
			$postType = $this->getPostType();
			$postType->set_slug('event')
					->set_name('Events')
					->set_singular('Event')
					->set_plural('Events')
					->set_menu_name('Events')
					->set_position(501)
					->add_support('editor')
					//->add_support('author')
					->add_support('page-attributes')
					->add_support('thumbnail')
					//->add_support('excerpt')
					->set_hierarchical(true)
					->set_label('add_new', 'Add New Event')
					->set_label('add_new_item', 'Add New Event')
					//->set_show_in_menu(false)
					//->show_ui(false)
					->set_capability_type('post')
					;
			return $postType;
		}
		
		public function setupTaxonomy() {
			
			$location = new Custom_Taxonomy( $this->getId() );
			$location->set_id('realestate-location')
					->set_name('Location')
					->set_plural('Locations')
					->set_menu_name('Locations')
					->set_slug('realestate-location')
					->init();
			
			$location = new Custom_Taxonomy( $this->getId() );
			$location->set_id('realestate-type')
					->set_name('Property Type')
					->set_plural('Property Types')
					->set_menu_name('Property Types')
					->set_slug('property-type')
					->init();
						
			$post_tag = new Custom_Taxonomy( $this->getId() );
			$post_tag->set_id('post_tag')
						->init();
		}
		
		public function setupMetaboxes() {
			if ( !class_exists('Custom_Metabox') ) {
				return;
			}		
		}
		
		public function rowActions( $actions, $post ) {
			$actions['view'] = str_replace('href', 'target="_blank" href', $actions['view']);
			return $actions;
		}
		
		public function adminBar() 
		{
			if( current_user_can('subscriber') ) {
				return;
			}
			global $wp_admin_bar;  
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_parent', 
				'title' => 'Events',
				'href' => admin_url() . 'edit.php?post_type=' . $this->getId(),
			)); 
			
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_add', 
				'title' => 'Add New',
				'href' => admin_url() . 'post-new.php?post_type=' . $this->getId(),
				'parent' => $this->getId() . '_parent',
			)); 
			if( !current_user_can('contributor')  && !current_user_can('author')) {
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_categories', 
				'title' => 'Property Types',
				'href' => admin_url() . 'edit-tags.php?post_type=' . $this->getId() . '&taxonomy=realestate-type',
				'parent' => $this->getId() . '_parent',
			));
			
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_location', 
				'title' => 'Locations',
				'href' => admin_url() . 'edit-tags.php?post_type=' . $this->getId() . '&taxonomy=realestate-location',
				'parent' => $this->getId() . '_parent',
			));
			
			$wp_admin_bar->add_node(array(
				'id' => $this->getId() . '_post_tag', 
				'title' => 'Tags',
				'href' => admin_url() . 'edit-tags.php?post_type=' . $this->getId() . '&taxonomy=post_tag',
				'parent' => $this->getId() . '_parent',
			));
			}
		}
	}
}
