<?php

add_action( 'wp_ajax_backendapi', 'backendapi_callback' );
add_action( 'wp_ajax_nopriv_backendapi', 'backendapi_callback' );

function backendapi_callback() {
	header("Access-Control-Allow-Origin: *");

	
	if( (! isset($_REQUEST['licence_key']) ) || (isset($_REQUEST['licence_key']) && ($_REQUEST['licence_key'] !== '75a1752ce9addaeb1f811d518413978a') )) {
		wp_die();
	}
	
	if( isset($_REQUEST['json']) && ($_REQUEST['json'] != '') ) {
		switch( $_REQUEST['json'] ) {
			case 'establishment-categories':
				backendapi_establishment_categories();
			break;
			/* Delete Soon */
			case 'category':
				if( isset($_REQUEST['id']) && ($_REQUEST['id'] != '') ) {
					$offset = ( isset($_REQUEST['offset']) && ($_REQUEST['offset'] != '') ) ? $_REQUEST['offset'] : 0;
					$limit = ( isset($_REQUEST['limit']) && ($_REQUEST['limit'] != '') ) ? $_REQUEST['limit'] : 20;
					backendapi_category( $_REQUEST['id'], $offset, $limit );
				}
			break;
			case 'search':
				if( (isset($_REQUEST['key']) && ($_REQUEST['key'] != '') ) && ( isset($_REQUEST['category']) && ($_REQUEST['category'] != '') )) {
					$offset = ( isset($_REQUEST['offset']) && ($_REQUEST['offset'] != '') ) ? $_REQUEST['offset'] : 0;
					$limit = ( isset($_REQUEST['limit']) && ($_REQUEST['limit'] != '') ) ? $_REQUEST['limit'] : 20;
					backendapi_search($_REQUEST['key'], $_REQUEST['category'], $offset, $limit);
				}
			break;
			/* end Delete here */
			case 'establishments-list':
				if( isset($_REQUEST['id']) && ($_REQUEST['id'] != '') ) {
					$offset = ( isset($_REQUEST['offset']) && ($_REQUEST['offset'] != '') ) ? $_REQUEST['offset'] : 0;
					$limit = ( isset($_REQUEST['limit']) && ($_REQUEST['limit'] != '') ) ? $_REQUEST['limit'] : 20;
					backendapi_category( $_REQUEST['id'], $offset, $limit );
				}
			break;
			case 'search':
				if( (isset($_REQUEST['key']) && ($_REQUEST['key'] != '') ) && ( isset($_REQUEST['category']) && ($_REQUEST['category'] != '') )) {
					$offset = ( isset($_REQUEST['offset']) && ($_REQUEST['offset'] != '') ) ? $_REQUEST['offset'] : 0;
					$limit = ( isset($_REQUEST['limit']) && ($_REQUEST['limit'] != '') ) ? $_REQUEST['limit'] : 20;
					backendapi_search($_REQUEST['key'], $_REQUEST['category'], $offset, $limit);
				}
			break;
			case 'establishment':
				if( isset($_REQUEST['id']) && ($_REQUEST['id'] != '') ) {
					backendapi_establishment( $_REQUEST['id'] );
				}
			break;
		}
	}
	wp_die();
}

function backendapi_establishment_categories() {
	$response = array();
	$categories = get_terms('establishment-category', array('hide_empty'=>false));
	
	foreach( $categories as $category ) {
		
		$response[] = array(
			'id'=>$category->term_id,
			'title'=>$category->name, 
			'description'=>$category->description, 
			'parent' => $category->parent,
			'count' => $category->count,
		);
	} 
	
	echo json_encode( $response );
}

function backendapi_category( $id, $offset=0, $posts_per_page=20 ) {
	$response = array();
	$items = get_posts( array( 
		'post_type'=>'establishment',
		'posts_per_page'   => $posts_per_page,
		'offset' => $offset,
		'tax_query' => array(
			array(
				'taxonomy' => 'establishment-category',
				'field' => 'term_id',
				'terms' => $id
			)
		),
		'orderby' => 'post_title',
		'order' => 'ASC',
	));
	
	$n=0;
	foreach( $items as $item ) {
		$address = implode(', ', array(
			 get_post_meta($item->ID, '_establishment_details_address', true),
			get_post_meta($item->ID, '_establishment_details_address_city', true),
			get_post_meta($item->ID, '_establishment_details_address_region', true),
			'Philippines',
			get_post_meta($item->ID, '_establishment_details_address_postal', true)
		));
		
		$phone = get_post_meta($item->ID, '_establishment_details_phone', true);
		$logo = wp_get_attachment_image_src( get_post_meta($item->ID, '_establishment_logo_logo', true), 'full' );
		
		$response[] = array(
			'id'=>$item->ID,
			'title'=>$item->post_title, 
			'address' => $address,
			'telephone' => $phone,
			'logo' => (is_array($logo) ? $logo[0] : false),
			'terms' => get_the_terms( $item->ID, 'establishment-category'),
			'permalink' => get_permalink( $item->ID ),
			);
		$n++;
	} 
	
	//$term = get_term( $id, 'establishment-category');
	
	echo json_encode( array(
		//'page_title'=> $term->name, 
		'items' => $response, 
		'id'=> $id,
		'offset' => ($offset + $posts_per_page), 
		'limit' => $posts_per_page,
		'count' => $n, 
		));
}

function backendapi_search( $key, $category, $offset=0, $posts_per_page=20 ) {
	$response = array();
	$args = array( 
		's' => $key,
		'post_type'=>'establishment',
		'posts_per_page'   => $posts_per_page,
		'offset' => $offset,
		'orderby' => 'post_title',
		'order' => 'ASC',
	);
	
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'establishment-category',
				'field' => 'term_id',
				'terms' => $category
			)
		);
	
	$items = get_posts($args);
	
	$n=0;
	foreach( $items as $item ) {
		$address = implode(', ', array(
			 get_post_meta($item->ID, '_establishment_details_address', true),
			get_post_meta($item->ID, '_establishment_details_address_city', true),
			get_post_meta($item->ID, '_establishment_details_address_region', true),
			'Philippines',
			get_post_meta($item->ID, '_establishment_details_address_postal', true)
		));
		
		$phone = get_post_meta($item->ID, '_establishment_details_phone', true);
		$logo = wp_get_attachment_image_src( get_post_meta($item->ID, '_establishment_logo_logo', true), 'full' );
		
		$response[] = array(
			'id'=>$item->ID,
			'title'=>$item->post_title, 
			'address' => $address,
			'telephone' => $phone,
			'logo' => (is_array($logo) ? $logo[0] : false),
			'terms' => get_the_terms( $item->ID, 'establishment-category')
			);
		$n++;
	} 
	
	//$term = get_term( $category, 'establishment-category');
	
	echo json_encode( array(
		//'page_title'=> $term->name, 
		'items' => $response, 
		'id'=> $id,
		'offset' => ($offset + $posts_per_page), 
		'limit' => $posts_per_page,
		'count' => $n, 
		));
}

function backendapi_establishment( $id ) {
	$post = get_post( $id );
	if( ! $post ) { 
		echo json_encode( false );
		exit;
	}
	$street = get_post_meta($id, '_establishment_details_address', true);
	$address = implode(', ', array(
		$street,
		get_post_meta($id, '_establishment_details_address_city', true),
		get_post_meta($id, '_establishment_details_address_region', true),
		'Philippines',
		get_post_meta($id, '_establishment_details_address_postal', true)
	));
	$phone = get_post_meta($id, '_establishment_details_phone', true);
	$mobile = get_post_meta($id, '_establishment_details_mobile', true); 
	$fax = get_post_meta($id, '_establishment_details_fax', true); 
	$website = get_post_meta($id, '_establishment_details_website', true); 
	$mapgeo = get_post_meta($id, '_establishment_details_mapgeo', true); 
	$logo = wp_get_attachment_image_src( get_post_meta($item->ID, '_establishment_logo_logo', true), 'full' );
	$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($id, 'thumbnail') );
	
	$response = array(
		// details
		'id' => $id,
		'title' => $post->post_title,
		'permalink' => get_permalink( $id ),
		'address' => $address,
		'telephone' => $phone,
		'mobile' => $mobile,
		'fax' => $fax,
		'website' => $website,
		'thumbnail' => (($thumbnail)?$thumbnail:''),
		'mapgeo' => $mapgeo,
		'logo' => (($logo)?$logo:''),
		'flickr_photoset' => get_post_meta($id, '_establishment_details_flickr_photoset', true),
		'flickr_userid' => get_post_meta($id, '_establishment_details_flickr_userid', true),
		
		// advertising
		'call_active' => get_post_meta($id, '_establishment_advertising_call_active', true),
		'email_active' => get_post_meta($id, '_establishment_advertising_email_active', true),
		'primary_phone' => get_post_meta($id, '_establishment_advertising_phone_primary', true),
		'primary_email' => get_post_meta($id, '_establishment_advertising_email_primary', true),
		'share_active' => get_post_meta($id, '_establishment_advertising_share_active', true),
		'reviews_active' => get_post_meta($id, '_establishment_advertising_reviews_active', true),
		'map_active' => get_post_meta($id, '_establishment_advertising_map_active', true),
		'gallery_active' => get_post_meta($id, '_establishment_advertising_gallery_active', true),
		'favorite_active' => get_post_meta($id, '_establishment_advertising_favorite_active', true),
		'verified' => get_post_meta($id, '_establishment_advertising_verified', true),
		'terms' => get_the_terms( $id, 'establishment-category'),
		
	);
	
	echo json_encode( $response );
}
