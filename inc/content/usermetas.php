<?php
add_action('admin_init', 'indavao_add_user_meta');
function indavao_add_user_meta() {
	if (class_exists( 'Custom_User_Meta' )) {
		if( current_user_can('create_users') ) {
			$usermeta = new Custom_User_Meta(false);
			$usermeta->set_label('User Privileges');
			$usermeta->add_field(array('label' => 'Local Business', 'meta_key'=> '_indavao_localbusiness', 'meta_value'=> '', 'desc'=>'Can See Local Business', 'type' => 'checkbox'));
			$usermeta->add_field(array('label' => 'Real Estate', 'meta_key'=> '_indavao_realestate', 'meta_value'=> '', 'desc'=>'Can See Real Estate', 'type' => 'checkbox'));
			$usermeta->add_field(array('label' => 'Jobs', 'meta_key'=> '_indavao_jobs', 'meta_value'=> '', 'desc'=>'Can See Jobs', 'type' => 'checkbox'));
			$usermeta->add_field(array('label' => 'Cars', 'meta_key'=> '_indavao_cars', 'meta_value'=> '', 'desc'=>'Can See Cars', 'type' => 'checkbox'));
			$usermeta->add_field(array('label' => 'Motorcycles', 'meta_key'=> '_indavao_motorcycles', 'meta_value'=> '', 'desc'=>'Can See Motorcycles', 'type' => 'checkbox'));
			$usermeta->add_field(array('label' => 'Events', 'meta_key'=> '_indavao_events', 'meta_value'=> '', 'desc'=>'Can See Events', 'type' => 'checkbox'));
			$usermeta->init();
		}
	}
}
