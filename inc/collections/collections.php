<?php

class InDavaoCollections extends Custom_Admin_Page {
	
	public $id = 'indavao_collections';
	public $title = 'Collections';
	public $menu_name = 'Collections';
	public $permission = 'read';
	public $icon = 'collection.png';
	public $priority = '290';
	
	function admin_page() {
		global $plugin_dir;
echo <<<HTML
		<div class="wrap" style="padding-top:20px;">

<div class="indavao-my-network theme-browser rendered">
	<div class="themes">
HTML;

$collections = array(
	array(
		'id' => 'fb_groups',
		'name' => 'Facebook Groups',
		'image' => plugins_url($plugin_dir."/images/facebook-groups.png")
	),
);

foreach( $collections as $collection ) {
	$screenshot = ($collection['image']!='')?"<img src=\"{$collection['image']}\" alt=\"\">":'';
	$image_class = ($collection['image']!='')?"":'blank';
	$button_url = admin_url('admin.php?page=' . $this->id . '_' . $collection['id']);
	
echo <<<HTML
<div class="theme">
<div class="theme-screenshot {$image_class}">{$screenshot}</div>
<span class="more-details">{$collection['name']}</span>
<h3 class="theme-name">{$collection['name']} </h3>
<div class="theme-actions">
	<a class="button button-primary" href="{$button_url}">Learn More</a>
</div></div>
HTML;
}

echo <<<HTML
	</div>
<br class="clear"></div>

		</div>
HTML;
	}
	
}
