<?php

class InDavaoCollectionsMyFriends extends Custom_Sub_Admin_Page {
	public $id = 'indavao_collections_my_friends';
	public $title = 'My Facebook Friends';
	public $menu_name = 'My Facebook Friends';
	public $permission = 'read';
	public $admin_footer = true;
	
	function admin_page() {
		$facebook_app_id = get_user_meta(get_current_user_id(), '_indavao_facebook_app_id', true);
echo <<<PHP
			<div class="wrap">
				<h2>{$this->title}
PHP;
if( $facebook_app_id ) {
echo <<<PHP
				<div class="fb-login-button" data-scope="user_friends" data-max-rows="1" data-show-faces="false" data-auto-logout-link="true">Login to Download Friends</div>
				</h2>
				
<button id="add-my-facebook-friends" class="button button-primary hidden">Download My Friends</button>

PHP;
} else {
	echo "</h2>";
	$link = admin_url('admin.php?page=indavao_my_settings');
	echo "You have not set your Facebook App ID... <a href='{$link}'>Set Now!</a>";
}
echo <<<PHP
		</div>
PHP;
	}
	
	function admin_footer() {
		if( $this->not_this_page() ) { return; }
		$facebook_app_id = get_user_meta(get_current_user_id(), '_indavao_facebook_app_id', true);
echo <<<JS
<script>
	
window.fbAsyncInit = function() {
  FB.init({
    appId      : '{$facebook_app_id}',
    cookie     : true,  
    xfbml      : true,  
    version    : 'v2.2'
 });
  FB.getLoginStatus(function(response) {
(function($, FB){
	$('#add-my-facebook-friends').removeClass('hidden');
	$('#add-my-facebook-friends').click(function(){
		FB.api('/me/friends',  function(response) {
		  console.log( response );
	  });
	});
})(jQuery, FB);
	
	
  });
};
	  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
</script>
JS;
	}
}
