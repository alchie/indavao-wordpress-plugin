<?php

class InDavaoCollectionsFBGroups extends Custom_Sub_Admin_Page {
	public $id = 'indavao_collections_fb_groups';
	public $title = 'Facebook Groups';
	public $menu_name = 'Facebook Groups';
	public $permission = 'read';
	public $admin_bar = false;
	
	function admin_page() {
		$link = admin_url('admin.php?page=' . $this->id);
echo <<<HTML
		<div class="wrap">
		<h2>{$this->title}</h2>
HTML;

$cities = get_option("_indavao_manage_fb_groups_data");

if( $cities ) {
echo "<div class=\"group-collections\">";
foreach( $cities as $city ) {
	echo <<<CITY
<a href="{$city['collection_url']}" target="_blank" class="group-item">
<span class="icon"></span>
<strong>{$city['collection_name']}</strong>
<span class="count">{$city['collection_count']} groups</span></a>
CITY;
}
echo "</div>";
}

echo "</div>";

	}
	
}
