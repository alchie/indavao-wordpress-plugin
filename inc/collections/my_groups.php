<?php

class InDavaoCollectionsMyGroups extends Custom_Sub_Admin_Page {
	public $id = 'indavao_collections_my_groups';
	public $title = 'My Facebook Groups';
	public $menu_name = 'My Facebook Groups';
	public $permission = 'read';
	public $admin_footer = true;
	
function admin_page() {
		$facebook_app_id = get_user_meta(get_current_user_id(), '_indavao_facebook_app_id', true);
echo <<<PHP
			<div class="wrap">
				<h2>{$this->title}
PHP;
if( $facebook_app_id ) {
echo <<<PHP
				<div class="fb-login-button" data-scope="user_groups" data-max-rows="1" data-show-faces="false" data-auto-logout-link="true">Login to Download Friends</div>
				</h2>

<button id="add-my-facebook-groups" class="button button-primary hidden">Download My Groups</button>
PHP;
} else {
	echo "</h2>";
	$link = admin_url('admin.php?page=indavao_my_settings');
	echo "You have not set your Facebook App ID... <a href='{$link}'>Set Now!</a>";
}
echo <<<PHP
		</div>
PHP;
	}
	
	function admin_footer() {
		if( $this->not_this_page() ) { return; }
		$facebook_app_id = get_user_meta(get_current_user_id(), '_indavao_facebook_app_id', true);
echo <<<JS
<script>
	
window.fbAsyncInit = function() {
  FB.init({
    appId      : '{$facebook_app_id}',
    cookie     : true,  
    xfbml      : true,  
    version    : 'v2.2'
 });
 
  FB.getLoginStatus(function(response) {
(function($, FB){
	$('#add-my-facebook-groups').removeClass('hidden');
	$('#add-my-facebook-groups').click(function(){
		FB.api('/me/groups',  function(response) {
		  console.log( response );
		/* (function($, response) {
			
			$.ajax({
				method: 'POST',
				url : '<?php echo admin_url('admin-ajax.php?action=indavao_fb_groups'); ?>',
				data: { fb_user_id: response.id }
			}).done(function(resp) {
				console.log( resp );
				//location.reload();
			});
		})(jQuery, response); */
	  });
	});
})(jQuery, FB);
	
  });
};
	  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
</script>
JS;
	}
}
