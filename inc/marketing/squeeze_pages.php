<?php

class InDavaoMarketingSqueezePages extends Custom_Sub_Admin_Page {

	public $id = 'indavao_my_squeeze_pages';
	public $title = 'Squeeze Pages';
	public $menu_name = 'Squeeze Pages';
	public $permission = 'read';
	public $admin_bar = true;
	public $admin_footer = true;
	
	function __construct(Custom_Admin_Page $parent) {
		parent::__construct( $parent );
		add_action( 'wp_ajax_' . $this->id, array( &$this, 'save_short_url') );
	}
	
	function save_short_url() {
		if(( isset($_POST['shortURL']) && $_POST['shortURL'] != '') && ( isset($_POST['spId']) && $_POST['spId'] != '') ){
			global $current_user;
			update_user_meta($current_user->ID, '_indavao_squeeze_url_' . $_POST['spId'], $_POST['shortURL']);
		}
		exit;
	}
	
	function admin_page() {
	global $current_user;
echo <<<HTML
		<div class="wrap">
			<h2>{$this->title}</h2>

<div class="indavao-my-network theme-browser rendered">
	<div class="themes">
HTML;

$pages = get_option('_indavao_squeeze_pages');

if( is_array( $pages ) ) {
foreach( $pages as $key=>$page ) {
	$screenshot = ($page['page_screenshot']!='')?"<img src=\"{$page['page_screenshot']}\" alt=\"\">":'';
	$screenshot_class = ($page['page_screenshot']!='')?"":'blank';
	$ref = site_url('?ref=' . $current_user->ID);
	$short = get_user_meta($current_user->ID, '_indavao_squeeze_url_' . $page['page_id'], true);
	$button_class="";
	$button2_class='hidden';
	if( $short ) {
		$button_class="hidden";
		$button2_class='';
	}
echo <<<HTML
<div class="theme">
<div class="theme-screenshot {$screenshot_class}">{$screenshot}</div>
<span class="more-details">Your Link<br><!--<input type="text" value="{$ref}&squeeze={$page['page_id']}" style="width:100%">
Short URL<br>--><input onclick="this.select();" type="text" value="{$short}" style="width:100%" id="squeeze_page_shorturl_{$page['page_id']}">
</span>
<h3 class="theme-name">{$page['page_name']}</h3>
<div class="theme-actions">
	<a id="getShortUrl{$page['page_id']}" class="button button-primary get-shorturl {$button_class}" data-sp="{$page['page_id']}" data-url="{$ref}&squeeze={$page['page_id']}" data-id="squeeze_page_shorturl_{$page['page_id']}" href="javascript:void(0);">Get Short URL</a>
	<a id="openLink{$page['page_id']}" class="button button-primary {$button2_class}" href="{$short}" target="_blank">Open Link</a>
</div>

</div>
HTML;
}
}

echo <<<HTML
	</div>
<br class="clear"></div>

		</div>
HTML;
	}
	
		function admin_footer() {
		if( $this->is_this_page() ) {
			$ajax_url = admin_url('admin-ajax.php?action=' .  $this->id);
echo <<<JS
<script>
(function($){
	$('.get-shorturl').click(function(){
		var id = $(this).attr('data-id');
		var sp = $(this).attr('data-sp');
		var longURL =  $(this).attr('data-url');
		var save_url = function(shortURL) {
			$.ajax({
				url: '{$ajax_url}',
				type: 'POST',
				data: { shortURL: shortURL, spId : sp },
				success: function(response) {
					//console.log( response );					
				}
			 });
		};
		$.ajax({
			url: 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyDBLtVV2Y6edCYAYYwf0WZgDn0Y5V0RqDs',
			type: 'POST',
			contentType: 'application/json; charset=utf-8',
			data: '{ longUrl: "' + longURL +'"}',
			success: function(response) {
				$('#'+id).val(response.id);
				$('#getShortUrl'+sp).addClass('hidden');
				$('#openLink'+sp).attr('href', response.id).removeClass('hidden');
				save_url( response.id );
			}
		 });
	});
})(jQuery);
</script>
JS;
		}
	}
}
