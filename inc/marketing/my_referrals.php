<?php

class InDavaoMarketingMyReferrals extends Custom_Admin_Page {

	public $id = 'indavao_my_referrals';
	public $title = 'My Referrals';
	public $menu_name = 'My Referrals';
	public $permission = 'read';
	public $icon = 'referrals.png';
	public $priority = '430';
	public $admin_bar = true;
	
	function admin_page() {
		global $current_user;
		$referral_link = site_url('?ref=' . $current_user->ID);
		
$referrals = get_users( array( 
'meta_key' => 'sponsor_id',
'meta_value' => $current_user->ID,
'orderby' => 'user_registered',
'order' => 'DESC',
) );

$links_redirection = admin_url('admin.php?page=indavao_my_links_redirections');
		echo <<<HTML
		<div class="wrap">
			<h2>{$this->title}
			<a href="{$links_redirection}" class="add-new-h2">Get Links</a>
			</h2>
HTML;
if( $referrals ) {
echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column" style="padding-left:10px">Username</th>
		<th scope="col"  class="manage-column" style="">Email</th>
		<th scope="col"  class="manage-column" style="">First Name</th>
		<th scope="col"  class="manage-column" style="">Last Name</th>
		<th scope="col" class="manage-column" style="">Phone Number</th>
		<th scope="col"  class="manage-column" style="">Date Signed Up</th>	
		<th scope="col"  class="manage-column" style=""></th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;

foreach( $referrals as $referral ) {
	$first_name = get_user_meta($referral->ID, 'first_name', true);
	$last_name = get_user_meta($referral->ID, 'last_name', true);
	$phone_number = get_user_meta($referral->ID, 'phone_number', true);
	$author_url = get_author_posts_url($referral->ID);
echo <<<HTML
<tr>
	<th>{$referral->user_login}</th>
	<td>{$referral->user_email}</td>
	<td>{$first_name}</td>
	<td>{$last_name}</td>
	<td>{$phone_number}</td>
	<td>{$referral->user_registered}</td>
	<td align="right"><a href="{$author_url}" target="_blank">Open Page</a></td>
</tr>
HTML;
}
		

echo <<<HTML
	</tbody>
</table>
		</div>
HTML;
} else {
	echo "You haven't referred anyone yet!";
}

	}
}
