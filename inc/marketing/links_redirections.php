<?php

class InDavaoMarketingLinksRedirection extends Custom_Sub_Admin_Page {

	public $id = 'indavao_my_links_redirections';
	public $title = 'Referral Links &amp; Redirections';
	public $menu_name = 'Links &amp; Redirections';
	public $permission = 'read';
	public $icon = 'networking.png';
	public $priority = '431';
	public $admin_bar = true;
	public $admin_init = true;
	public $admin_footer = true;
		
	function admin_page() {
		global $current_user, $plugin_dir;
		$payment_verified = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);
		$redirection_default = get_user_meta($current_user->ID, '_indavao_user_referral_redirection_default', true);
		$shorturl_default = get_user_meta($current_user->ID, '_indavao_user_referral_shorturl_default', true);
		
		$referral_link = site_url('?ref=' . $current_user->ID);
		$nonce = wp_nonce_field( $this->id );
		
		$refresh_img = plugins_url($plugin_dir.'/images/refresh.png');
		echo <<<HTML
		<div class="wrap">
			<h2>{$this->title}</h2>
HTML;
if( $_GET['success'] == 1 ) {
	$this->notification( "Changes Saved!" );
}
echo <<<HTML
<form method="post">
{$nonce}
<table class="form-table">
	<tbody>

<tr class="user-first-name-wrap">
	<th><label for="referral_link">Your Referral Link</label></th>
	<td><input disabled id="referral_link" class="regular-text" type="text" value="{$referral_link}" style="background-color: #FFF;color: #000;">
	<input name="shorturl_default" id="short-url-default" placeholder="Short URL Here..." class="regular-text" type="text" value="{$shorturl_default}">
	<a href="javascript:void(0);" data-url="{$referral_link}" data-id="short-url-default" class="get-shorturl" title="Get Google Short URL"><img src="{$refresh_img}"></a>
	<p class="description">If the `ref` query string is present, whichever the page maybe, a cookie will be save for 30 days.</p>
	</td>
</tr>

<tr class="user-first-name-wrap">
	<th><label for="referral_link">Default Redirection URL</label></th>
	<td><input name="redirection_default" class="regular-text"  placeholder="Redirection URL Here..." type="text" value="{$redirection_default}">
	<p class="description">Leave `blank` to remove redirection</p>
	</td>
</tr>
HTML;
for( $i=1;$i < 11; $i++ ) {
	$redirection = get_user_meta($current_user->ID, '_indavao_user_referral_redirection_'.$i, true);
	$shorturl = get_user_meta($current_user->ID, '_indavao_user_referral_shorturl_'.$i, true);
	
echo <<<HTML
<tr class="user-first-name-wrap">
	<th><label for="referral_link">Redirection {$i}</label></th>
	<td><input name="redirection_{$i}" id="referral_link-{$i}" placeholder="Redirection URL Here..." class="regular-text" type="text" value="{$redirection}">
	<input name="shorturl_{$i}" id="short-url-{$i}" class="regular-text" placeholder="Short URL Here..." type="text" value="{$shorturl}">
	<a href="javascript:void(0);" data-url="{$referral_link}&amp;redir={$i}" data-id="short-url-{$i}" class="get-shorturl" title="Get Google Short URL"><img src="{$refresh_img}"></a>
	<p class="description"><a href="{$referral_link}&redir={$i}" target="_blank">{$referral_link}&amp;redir={$i}</a></p>
	</td>
</tr>
HTML;

}

echo <<<HTML
</tbody></table>
HTML;

if( $payment_verified ) {
echo '<p><input class="button button-primary" type="submit" value="Save Changes"></p>';
} else {
echo '<p><a class="button button-primary" href="'.admin_url('admin.php?page=indavao_join_now').'">Join Now</a><br>
Redirections are only available to Premium Members
</p>';
}

echo <<<HTML
</form>
HTML;
	}
	
	function admin_footer() {
		if( isset($_GET['page']) && $_GET['page'] == 'indavao_my_referral_links_redirections') {
echo <<<JS
<script>
(function($){
	$('.get-shorturl').click(function(){
		var id = $(this).attr('data-id');
		var longURL =  $(this).attr('data-url');
		$.ajax({
			url: 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyDBLtVV2Y6edCYAYYwf0WZgDn0Y5V0RqDs',
			type: 'POST',
			contentType: 'application/json; charset=utf-8',
			data: '{ longUrl: "' + longURL +'"}',
			success: function(response) {
				$('#'+id).val(response.id);
			}
		 });
	});
})(jQuery);
</script>
JS;
		}
	}
	
	function admin_init() {
		if( !isset( $_GET['page'] ) || $_GET['page'] != $this->id ) {
			return;
		}
		global $current_user;
		$payment_verified = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);
		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) && $payment_verified) {
			update_user_meta($current_user->ID, '_indavao_user_referral_redirection_default', $_POST['redirection_default']);
			update_user_meta($current_user->ID, '_indavao_user_referral_shorturl_default', $_POST['shorturl_default']);
			for( $i=1;$i < 11; $i++ ) {
				update_user_meta($current_user->ID, '_indavao_user_referral_redirection_'.$i, $_POST['redirection_'.$i]);
				update_user_meta($current_user->ID, '_indavao_user_referral_shorturl_'.$i, $_POST['shorturl_'.$i]);
			}
			header("location: ". admin_url('admin.php?page=' . $this->id . "&success=1"));
			exit;
		}
		
	}
}
