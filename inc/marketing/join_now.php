<?php

class InDavaoMarketingJoinNow extends Custom_Admin_Page {

		public $id = 'indavao_join_now';
		public $title = 'Join Today';
		public $menu_name = 'Join Today';
		public $permission = 'read';
		public $icon = 'join_now.png';
		public $priority = '999';
		public $admin_bar = true;
		public $admin_init = true;
		
		function admin_menu() {
			global $current_user;
			$user_joined = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);
			if( ! $user_joined && !current_user_can('manage_options') ) {
				parent::admin_menu();
			}
		}
		
		function admin_bar_menu( $wp_admin_bar ) {
			global $current_user;
			$user_joined = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);
			if( ! $user_joined && !current_user_can('manage_options')) {
				parent::admin_bar_menu( $wp_admin_bar );
			}
		}
		
		function admin_page() {
			
			$nonce = wp_nonce_field( $this->id );
			global $current_user;
			$bank = get_user_meta($current_user->ID, '_indavao_user_payment_bank', true);
			$ref = get_user_meta($current_user->ID, '_indavao_user_payment_ref_id', true);
			$date = get_user_meta($current_user->ID, '_indavao_user_payment_datetime', true);

echo <<<HTML
<div class="wrap">
	<h2>Join Today</h2>
HTML;
if( $_GET['success'] == 1 ) {
	$this->notification( "Payment Details Submitted!", '', "We will activate your account and contact you as soon as we verify your payment! Thank you for joining us!" );
}
echo <<<HTML
		<form method="post">
			{$nonce}
			<input name="action" type="hidden" value="{$this->id}_save" class="regular-text">
		<table class="form-table">
		<tbody>
		<tr>
			<th scope="row"><label for="join_now_bank">Bank Name &amp; Branch</label></th>
			<td><input name="join_now_bank" type="text" id="join_now_bank" value="{$bank}" class="regular-text"></td>
		</tr>
		<tr>
			<th scope="row"><label for="join_now_reference">Deposit Reference ID</label></th>
			<td><input name="join_now_reference" type="text" id="join_now_reference" value="{$ref}" class="regular-text"></td>
		</tr>
		<tr>
			<th scope="row"><label for="join_now_datetime">Date &amp; Time</label></th>
			<td><input name="join_now_datetime" type="text" id="join_now_datetime" value="{$date}" class="regular-text"></td>
		</tr>
			</tbody></table>
			
		<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Submit Deposit Details"></p>
		</form>

</div>
HTML;
		}
		
		function admin_init() {
			if( $this->is_this_page() ) {
				$this->save_payment_details();
			}
		}
		
		function save_payment_details() {
		
			if( isset( $_POST['action'] ) && $_POST['action'] == $this->id.'_save') {
				if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {

					global $current_user;
					update_user_meta($current_user->ID, '_indavao_user_payment_bank', $_POST['join_now_bank']);
					update_user_meta($current_user->ID, '_indavao_user_payment_ref_id', $_POST['join_now_reference']);
					update_user_meta($current_user->ID, '_indavao_user_payment_datetime', $_POST['join_now_datetime']);
					header("location: ". admin_url('admin.php?page=' . $this->id . "&success=1"));
					exit;
					
				}
			} 
		
	}
		
}

