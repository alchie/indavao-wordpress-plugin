<?php

class InDavaoNetworkingNetworkManager extends Custom_Admin_Page {

		public $id = 'indavao_manage_network';
		public $title = 'Manage Network';
		public $menu_name = 'Manage Network';
		public $permission = 'edit_posts';
		public $icon = 'network_manager3.png';
		public $priority = '460';
		public $admin_bar = true;
		public $manage_team = false;
		
		function admin_menu() {
			global $current_user;
			$this->manage_team = get_user_meta($current_user->ID, '_indavao_user_can_manage_team', true);

			if( current_user_can('edit_posts') && $this->manage_team ) {
				parent::admin_menu();
			}
		}
		
		function admin_bar_menu( $wp_admin_bar ) {
			global $current_user;
			$manage_team = get_user_meta($current_user->ID, '_indavao_user_can_manage_team', true);

			if( current_user_can('edit_posts') && $manage_team ) {
				parent::admin_bar_menu( $wp_admin_bar );
			}
		}
		
		function admin_page() {
global $current_user, $plugin_dir;
$link = admin_url( 'admin.php?page=' . $this->id );
$join_now = admin_url( 'admin.php?page=indavao_join_now' );
$tables = get_option('_indavao_networking_mlm_tables');
$payment_verified = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);

		echo <<<HTML
		<div class="wrap">
			<h2>My Network</h2>

<div class="indavao-my-network theme-browser rendered">
	<div class="themes">
HTML;


if( is_array( $tables ) ) {
	foreach( $tables as $tbl ) {		

	$button = '<a class="button button-primary" href="'.admin_url('admin.php?page=indavao_manage_network_tree_'.$tbl['table_id']).'">Manage</a>';
	$table_img = plugins_url($plugin_dir."/images/table_1.png");
	$screenshot = ($tbl['table_screenshot']!='')?"<img src=\"{$tbl['table_screenshot']}\" alt=\"\">":"<img src=\"{$table_img}\" alt=\"\">";
$table_earnings = number_format( $tbl['table_earnings'] );
echo <<<HTML
<div class="theme {$table_exit_class}">
<div class="theme-screenshot">{$screenshot}</div>
<span class="more-details">{$tbl['table_name']}<br><em>(&#x20B1; {$table_earnings})</em></span>
<h3 class="theme-name">{$tbl['table_name']} <em>(&#x20B1; {$table_earnings})</em></h3>
<div class="theme-actions">
	{$button}
</div></div>
HTML;
}
}

echo <<<HTML
	</div>
<br class="clear"></div>

		</div>
HTML;
	}
}
