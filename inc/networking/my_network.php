<?php

class InDavaoNetworkingMyNetwork extends Custom_Admin_Page {

		public $id = 'indavao_my_network';
		public $title = 'My Network';
		public $menu_name = 'My Network';
		public $permission = 'read';
		public $icon = 'networking.png';
		public $priority = '450';
		public $admin_bar = true;
	
	function admin_page() {
global $current_user, $plugin_dir;
$link = admin_url( 'admin.php?page=' . $this->id );
$join_now = admin_url( 'admin.php?page=indavao_join_now' );
$tables = get_option('_indavao_networking_mlm_tables');
$payment_verified = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);

		echo <<<HTML
		<div class="wrap">
			<h2>My Network</h2>

<div class="indavao-my-network theme-browser rendered">
	<div class="themes">
HTML;


if( is_array( $tables ) ) {
	foreach( $tables as $tbl ) {		
		$button = '<a class="button button-primary" href="'.$join_now.'">Join Now</a>';
		$table_exit_class = '';
		
if( $payment_verified ) {
		$table_active = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$tbl['table_id'].'_active', true);
		$table_exit = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$tbl['table_id'].'_exit', true);
		
		$requirement = $tables[$tbl['table_requirement']];
		$req_active = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_active', true);
		$req_exit = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_exit', true);
		
		if($table_active) {
			if($requirement && ($req_exit && $req_active)) {
				$button = '<a class="button button-primary" href="'.admin_url('admin.php?page=indavao_my_network_tree_'.$tbl['table_id']).'">View Tree</a>';
			}
			if(!$requirement) {
				$button = '<a class="button button-primary" href="'.admin_url('admin.php?page=indavao_my_network_tree_'.$tbl['table_id']).'">View Tree</a>';
			}
		} 
		
		if( $requirement && $req_active && (!$req_exit)) {
			$button = '<a class="button" href="javascript:void(0);">'.$requirement['table_name'].' Required</a>';
		}
		
		if( $table_exit ) {
			$table_exit_class = 'active';
			$button = '<a class="button" href="javascript:void(0);">Completed</a>';
		}
}
	$table_img = plugins_url($plugin_dir."/images/table_1.png");
	$screenshot = ($tbl['table_screenshot']!='')?"<img src=\"{$tbl['table_screenshot']}\" alt=\"\">":"<img src=\"{$table_img}\" alt=\"\">";
$table_earnings = number_format( $tbl['table_earnings'] );
echo <<<HTML
<div class="theme {$table_exit_class}">
<div class="theme-screenshot">{$screenshot}</div>
<span class="more-details">{$tbl['table_name']}<br><em>(&#x20B1; {$table_earnings})</em></span>
<h3 class="theme-name">{$tbl['table_name']} <em>(&#x20B1; {$table_earnings})</em></h3>
<div class="theme-actions">
	{$button}
</div></div>
HTML;

}
}

echo <<<HTML
	</div>
<br class="clear"></div>

		</div>
HTML;
	}
	
}

