<?php

class InDavaoNetworkingMLMTree extends Custom_Sub_Admin_Page {

	public $id = 'indavao_my_network_tree';
	public $title = 'My Network Tree';
	public $menu_name = 'My Network Tree';
	public $permission = 'read';
	public $icon = 'networking.png';
	public $priority = '431';
	public $admin_bar = true;
	
	function admin_menu() {
		global $current_user;
		$tables = get_option('_indavao_networking_mlm_tables');
		$payment_verified = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);
		if( $tables && $payment_verified ) {
			foreach( $tables as $table ) {
				$table_active = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$table['table_id'].'_active', true);
				$requirement = $tables[$table['table_requirement']];
				$req_active = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_active', true);
				$req_exit = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_exit', true);
				if(($table_active && ($requirement!=NULL && ($req_exit && $req_active))) || ($table_active && ($requirement==NULL))) {
					add_submenu_page( $this->parent->id, $table['table_name'], $table['table_name'], $this->permission, $this->id . '_' .$table['table_id'], array( &$this, 'admin_page') );
				}
			}
		}
	}
	
	function admin_page() {
		global $current_user;
		$screen = get_current_screen();
		$table_id = str_replace('my-network_page_indavao_my_network_tree_','',$screen->id);
		$tables = get_option('_indavao_networking_mlm_tables');
		$current_table = $tables[$table_id];
		$requirement = $tables[$current_table['table_requirement']];
		$req_exit = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_exit', true);
		$user_team = get_user_meta($current_user->ID, '_indavao_user_mlm_team', true);
		$users_combined = $this->_get_hierarchy_combined($current_user->ID, $table_id, $user_team);
		$table_earnings = number_format( $current_table['table_earnings'] );
echo <<<HTML
<div class="wrap">
<h2>{$current_table['table_name']} <em>(&#x20B1; {$table_earnings})</em></h2>

<div class="network container">
	<div class="row one-col">
		<div class="box you good">YOU</div>
	</div>
</div>

<div class="network container">
HTML;

$class="right";
foreach( $users_combined as $user ) {
if( $user['level'] === 1 ) {
	$good = 'good';
	if( $user['empty'] ) { $good=''; };
echo <<<HTML
	<div class="row two-col">
		<div class="line {$class}"></div>
		<div class="box {$good}">{$user['display_name']}</div>
	</div>
HTML;
	if( $class=="right" ) {
		$class="left";
	} else {
		$class="right";
	}
}
}

echo <<<HTML
</div>

<div class="network container">
HTML;

$class="right";
foreach( $users_combined as $user ) {
if( $user['level'] === 2 ) {
	$good = 'good';
	if( $user['empty'] ) {$good='';};
echo <<<HTML
	<div class="row four-col">
		<div class="line {$class}"></div>
		<div class="box {$good}">{$user['display_name']}</div>
	</div>
HTML;
	
	if( $class=="right" ) {
		$class="left";
	} else {
		$class="right";
	}
}
	
}

echo <<<HTML
</div>

<div class="network container">
HTML;

$class="right";
foreach( $users_combined as $user ) {
if( $user['level'] === 3 ) {
	$good = 'good';
	if( $user['empty'] ) {$good='';};
echo <<<HTML
	<div class="row eight-col bottom">
		<div class="line {$class}"></div>
		<div class="box {$good}">{$user['display_name']}</div>
	</div>
HTML;

	if( $class=="right" ) {
		$class="left";
	} else {
		$class="right";
	}
}
}

echo <<<HTML
</div>

</div>
HTML;
	}
	
	function _get_hierarchy_combined($user_id, $table, $user_team, $container = array(), $level = 0) {
		$results = array();
		if( $user_id ) {
			$results = get_users(array(
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => '_indavao_user_mlm_table_'.$table.'_parent_id',
						'value' => $user_id,
					),
					array(
						'key' => '_indavao_user_mlm_team',
						'value'=> $user_team
					)
				),
				'number' => 2
			));
		}
		$level++;
		$user_ids = array();
		for( $i=0; $i<=1;$i++ ) {
			if( isset( $results[$i] ) ) {
				$user_ids[] = $results[$i]->ID;
				$container[] = array(
					'level' => $level,
					'parent' => $user_id,
					'ID' => $results[$i]->ID,
					'display_name' => $results[$i]->data->display_name,
					'empty' => false
					);
			} else {
				$user_ids[] = false;
				$container[] = array(
					'level' => $level,
					'display_name' => 'Empty',
					'empty' => true
					);
			}
		}
		
		if( $level < 3 ) {
			foreach( $user_ids as $user_id) {
				$container = $this->_get_hierarchy_combined($user_id, $table, $user_team, $container, $level);
			}
		}
		
		/*
		foreach( $results as $result ) {
			 $container[] = array(
				'level' => $level,
				'parent' => $user_id,
				'ID' => $result->ID,
				'display_name' => $result->data->display_name,
			);
			if( $level < 3 ) {
				$container = $this->_get_hierarchy_combined($result->ID, $table, $user_team, $container, $level);
			}
		}*/
		return $container;
	}
	/*
	function _get_hierarchy_children($user_id, $user_team, $table, $level = 3) {
		$container = array();
		$results = get_users(array(
			'meta_key' => '_indavao_user_mlm_table_'.$table.'_parent_id',
			'meta_value' => $user_id,
			'number' => 2
		));
		foreach( $results as $result ) {
			 $container[] = array(
				'level' => $level,
				'parent' => $user_id,
				'ID' => $result->ID,
				'display_name' => $result->data->display_name,
			);
		}
		
		if( $level > 0 ) {
			$level--;
			foreach( $container as $key=>$data ) {
				$container[$key]['children'] = $this->_get_hierarchy_children($data['ID'], $table, $level);
			}	
		}
		
		return $container;
	}
	*/
	function admin_bar_menu( $wp_admin_bar ) {
		
		global $current_user;
		$tables = get_option('_indavao_networking_mlm_tables');
		$payment_verified = get_user_meta($current_user->ID, '_indavao_user_payment_verified', true);
		if( $tables && $payment_verified ) {
			foreach( $tables as $table ) {
				$table_active = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$table['table_id'].'_active', true);
				$table_exit = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$table['table_id'].'_exit', true);
				$requirement = $tables[$table['table_requirement']];
				$req_active = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_active', true);
				$req_exit = get_user_meta($current_user->ID, '_indavao_user_mlm_table_'.$requirement['table_id'].'_exit', true);
				
				if((!$table_exit) && (($table_active && ($requirement!=NULL && ($req_exit && $req_active))) || ($table_active && ($requirement==NULL)))) {
					$wp_admin_bar->add_node( array(
						'id' => 'sub_admin_bar_menu_' . $this->id . '_' .$table['table_id'],
						'title' => $table['table_name'],
						'parent' => 'admin_bar_menu_'.$this->parent->id,
						'href' => admin_url('admin.php?page='. $this->id . '_' .$table['table_id'])
					));
				}
			}
		}
		
	}
	
}
