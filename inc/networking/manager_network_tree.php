<?php

class InDavaoNetworkingNetworkTreeManager extends Custom_Sub_Admin_Page {

	public $id = 'indavao_manage_network_tree';
	public $title = 'Manage Network Tree';
	public $menu_name = 'Manage Network Tree';
	public $permission = 'edit_posts';
	public $icon = 'networking.png';
	public $priority = '431';
	public $admin_bar = false;
	public $admin_init = true;
	
	function admin_menu() {
		if( current_user_can('edit_posts') && $this->parent->manage_team ) {
			$tables = get_option('_indavao_networking_mlm_tables');
			foreach( $tables as $table ) {
				add_submenu_page( $this->parent->id, $table['table_name'], $table['table_name'], $this->parent->permission, $this->id . '_' .$table['table_id'], array( &$this, 'admin_page') );
			}
		}
	}

	function admin_page() {
		global $current_user;
		$screen = get_current_screen();
		$table_id = str_replace('manage-network_page_indavao_manage_network_tree_','',$screen->id);
		
		$team_id = get_user_meta($current_user->ID, '_indavao_user_mlm_team', true);
		
if( $_GET['updated'] == 1 ) {
	$this->notification( "Parent Updated!" );
}

		if(isset($_GET['view_tree']) && $_GET['view_tree'] != '') {
			$this->user_tree( $_GET['view_tree'], $table_id, $team_id );
		} else {
			$this->user_list( $current_user->ID, $table_id, $team_id );
		}
	}
	
	function admin_init() {
		if( current_user_can('edit_posts') && $this->parent->manage_team ) {
			$table_id = str_replace('indavao_manage_network_tree_', '', $_GET['page']);
			
			if( !isset($_POST['table_id']) || $table_id != $_POST['table_id'] ) {	
				return;
			}
			
			if( !isset( $_POST['action'] ) || $_POST['action'] != $this->id . '_update') {
				return;
			}

			if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], $this->id ) ) {
				if ( isset( $_POST['parent_id'] ) && $_POST['parent_id'] != '' ) {
					$userdata = get_userdata($_POST['parent_id']);
					if( ! $userdata ) {
						wp_die("User doesn't exists! Error Log recorded!", 403);
					}
					$payment_verified = get_user_meta($userdata->ID, '_indavao_user_payment_verified', true);
					if( ! $payment_verified ) {
						wp_die("User made no payment yet!", 403);
					}
					$active = get_user_meta($userdata->ID, '_indavao_user_mlm_table_'.$table_id.'_active', true);
					if( ! $active ) {
						wp_die("User Network not active!", 403);
					}
					$exit = get_user_meta($userdata->ID, '_indavao_user_mlm_table_'.$table_id.'_exit', true);
					if( $exit ) {
						wp_die("User is full!", 403);
					}
					$my_team = get_user_meta(get_current_user_id(), '_indavao_user_mlm_team', true);
					$parent_team = get_user_meta($userdata->ID, '_indavao_user_mlm_team', true);
					if( $parent_team == $my_team ) {
						update_user_meta($_GET['view_tree'], '_indavao_user_mlm_table_'.$table_id.'_parent_id', $_POST['parent_id']);
						header("location: ". admin_url("admin.php?page={$_GET['page']}&view_tree={$_GET['view_tree']}&updated=1"));
						exit;
					} else {
						wp_die("It is forbidden to transfer a user to another team! Error Log recorded!", 403);
					}
					
				}
			}
		}
	}
	
	function user_list( $user_id, $table_id, $team_id ) {
		
		$tables = get_option('_indavao_networking_mlm_tables');
		$current_table = $tables[$table_id];
		$teams = get_option('_indavao_networking_mlm_teams');
		$team = $teams[$team_id];
		$link = admin_url('admin.php?page=' . $this->id.'_'.$current_table['table_id']);
		
$limit = 20; // number of rows in page
$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
$offset = ( $pagenum - 1 ) * $limit;

echo <<<HTML
		<div class="wrap">
		<h2>Manage {$current_table['table_name']}
		</h2>
		
HTML;

 $user_query = new WP_User_Query( array( 
	'meta_query' => array(
		'relation' => 'AND',
		array(
			'key' => '_indavao_user_payment_verified',
			'value'=> 'on'
		),
		array(
			'key' => '_indavao_user_mlm_team',
			'value'=> $team_id
		),
		array(
			'key' => '_indavao_user_mlm_table_'.$table_id.'_active',
			'value'=> 'on'
		),
	), 
	'number'=> $limit,
	'offset'=> $offset,
	'orderby'=>'user_registered',
	'order'=>'ASC',
	'exclude'=>array($user_id),
));

if ( ! empty( $user_query->results ) ) {
echo <<<HTML
<table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col"  class="manage-column" style="padding-left:10px;width:30px;">ID</th>
		<th scope="col"  class="manage-column" style="">Username</th>
		<th scope="col"  class="manage-column" style="">Email</th>
		<th scope="col"  class="manage-column" style="">First Name</th>
		<th scope="col"  class="manage-column" style="">Last Name</th>
		<th scope="col" class="manage-column" style="">Phone Number</th>
		<th scope="col"  class="manage-column" style="">Date Signed Up</th>	
		<th scope="col"  class="manage-column" style="text-align:right">View Tree</th>
		</tr>
	</thead>
	<tbody id="the-list">
HTML;

foreach ( $user_query->results as $user ) {
	$first_name = get_user_meta($user->ID, 'first_name', true);
	$last_name = get_user_meta($user->ID, 'last_name', true);
	$phone_number = get_user_meta($user->ID, 'phone_number', true);
	$tree_url = add_query_arg( 'view_tree', $user->ID );
echo <<<HTML
<tr>
	<th>{$user->ID}</th>
	<th>{$user->user_login}</th>
	<td>{$user->user_email}</td>
	<td>{$first_name}</td>
	<td>{$last_name}</td>
	<td>{$phone_number}</td>
	<td>{$user->user_registered}</td>
	<td align="right"><a href="{$tree_url}">View Tree</a></td>
</tr>
HTML;
}
		

echo <<<HTML
	</tbody>
</table>
		
HTML;

$num_of_pages = ceil(  $user_query->get_total() / $limit );

$page_links = paginate_links( array(
    'base' => add_query_arg( 'pagenum', '%#%' ),
    'format' => '',
    'prev_text' => __( '&laquo;', 'text-domain' ),
    'next_text' => __( '&raquo;', 'text-domain' ),
    'total' => $num_of_pages,
    'current' => $pagenum
) );

if ( $page_links ) {
    echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
}

} else {
	echo "No one to manage yet!";
}

echo <<<HTML
</div>
HTML;
	}
	
	
	function user_tree( $user_id, $table_id, $team_id ) {
		
		$user_team = get_user_meta($user_id, '_indavao_user_mlm_team', true);
		
		if( $user_team != $team_id ) {
			wp_die("You do not have sufficient permissions to access this page.", 403);
		}
		
		$current_user = get_userdata( $user_id );
		$tables = get_option('_indavao_networking_mlm_tables');
		$current_table = $tables[$table_id];
		$requirement = $tables[$current_table['table_requirement']];
		$req_exit = get_user_meta($user_id, '_indavao_user_mlm_table_'.$requirement['table_id'].'_exit', true);
		$user_parent_id = get_user_meta($user_id, '_indavao_user_mlm_table_'.$table_id.'_parent_id', true);
		$user_parent_exit = get_user_meta($user_parent_id, '_indavao_user_mlm_table_'.$table_id.'_exit', true);
		
		$users_combined = $this->_get_hierarchy_combined($user_id, $table_id, $user_team);
		$nonce = wp_nonce_field( $this->id );
		$parent_link = admin_url("admin.php?page=indavao_manage_network_tree_{$table_id}&view_tree={$user_parent_id}&highlight={$user_id}");
$table_earnings = number_format( $current_table['table_earnings'] );
echo <<<HTML
<div class="wrap">
<h2>{$current_table['table_name']} <em>(&#x20B1; {$table_earnings})</em></h2>
HTML;
if( $user_parent_exit  ) {
	echo "<div class=\"network-tree parent\">
	<strong><a href=\"{$parent_link}\">PARENT</a> </strong>
	<br>User Locked because Parent User already made an Exit!</div>";
}
if( $user_id != get_current_user_id() && ! $user_parent_exit ) {
echo <<<HTML
<form method="post">
{$nonce}
<input name="action" type="hidden" value="{$this->id}_update">
<input name="table_id" type="hidden" value="{$table_id}">
<div class="network-tree parent">
<strong><a href="{$parent_link}">PARENT</a> ID</strong>
	<input size="3" type="text" name="parent_id" value="{$user_parent_id}" style="text-align:center">
	<button class="button button-primary" type="submit">Update</button>
</div>
</form>
HTML;
}

$top_display_name = $current_user->display_name;
if( $user_id == get_current_user_id() ) {
	$top_display_name = 'YOU';
}
echo <<<HTML
<div class="network container">
	<div class="row one-col">
		<div class="box you good"><span class="user-id">{$current_user->ID}</span>{$top_display_name}</div>
	</div>
</div>

<div class="network container">
HTML;

function tree_box($table_id, $user, $col='two-col', $class='right') {
		$good = 'good';
	$user_id = '<span class="user-id">'.$user['ID'].'</span>';
	$link = '<a class="tree-link" href="'.admin_url('admin.php?page=indavao_manage_network_tree_'.$table_id.'&view_tree='.$user['ID']).'">'.$user['display_name'].'</a>';
	$highlight = '';
	if( $user['empty'] ) { 
		$good=''; 
		$link = $user['display_name'];
		$user_id = '';
		};
	if( isset($_GET['highlight']) && $_GET['highlight'] == $user['ID'] ) {
		$highlight = 'highlight';
	}
echo <<<HTML
	<div class="row {$col}">
		<div class="line {$class}"></div>
		<div class="box {$good} {$highlight}">{$user_id}{$link}</div>
	</div>
HTML;
}

$class='right';
foreach( $users_combined as $user ) {
	if( $user['level'] === 1 ) {
		tree_box($table_id, $user, 'two-col' , $class);
		if( $class=="right" ) {
			$class="left";
		} else {
			$class="right";
		}
	}
}

echo <<<HTML
</div>

<div class="network container">
HTML;

$class="right";
foreach( $users_combined as $user ) {
if( $user['level'] === 2 ) {
	tree_box($table_id, $user, 'four-col' , $class);
	if( $class=="right" ) {
		$class="left";
	} else {
		$class="right";
	}
}
	
}

echo <<<HTML
</div>

<div class="network container">
HTML;

$class="right";
foreach( $users_combined as $user ) {
if( $user['level'] === 3 ) {
	tree_box($table_id, $user, 'eight-col bottom' , $class);
	if( $class=="right" ) {
		$class="left";
	} else {
		$class="right";
	}
}
}

echo <<<HTML
</div>

</div>
HTML;
	}
	
	function _get_hierarchy_combined($user_id, $table, $user_team, $container = array(), $level = 0) {
		$results = array();
		if( $user_id ) {
			$results = get_users(array(
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => '_indavao_user_mlm_table_'.$table.'_parent_id',
						'value' => $user_id,
					),
					array(
						'key' => '_indavao_user_mlm_team',
						'value'=> $user_team
					)
				),
				'number' => 2
			));
		}
		$level++;
		$user_ids = array();
		for( $i=0; $i<=1;$i++ ) {
			if( isset( $results[$i] ) ) {
				$user_ids[] = $results[$i]->ID;
				$container[] = array(
					'level' => $level,
					'parent' => $user_id,
					'ID' => $results[$i]->ID,
					'display_name' => $results[$i]->data->display_name,
					'empty' => false
					);
			} else {
				$user_ids[] = false;
				$container[] = array(
					'level' => $level,
					'display_name' => 'Empty',
					'empty' => true
					);
			}
		}
		
		if( $level < 3 ) {
			foreach( $user_ids as $user_id) {
				$container = $this->_get_hierarchy_combined($user_id, $table, $user_team, $container, $level);
			}
		}
		
		return $container;
	}
}
