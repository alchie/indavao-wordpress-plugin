<?php

class InDavaoMarketingFBPAP extends Custom_Sub_Admin_Page {
	public $id = 'indavao_marketing_tools_fbpap';
	public $title = 'Facebook Page Auto Poster';
	public $menu_name = 'FB Page Auto Poster';
	public $permission = 'read';
	public $admin_bar = false;
	public $priority = '9500';
	
	function admin_page() {
		global $current_user, $plugin_dir;
		$link = admin_url("admin.php?page=" . $this->id);
		$bg = plugins_url($plugin_dir . "/images/tool_smsac2.png");
echo <<<HTML
		<div class="wrap">
		<h2 class="banner-header" style="background:url({$bg}) no-repeat;">{$this->title}</h2>
		<p>An android app that lets you send SMS campaigns to all your contacts.</p>
HTML;

if( $_GET['updated'] == 1 ) {
	$this->notification( "Device Removed!" );
}

		$uuid = get_user_meta($current_user->ID, '_indavao_tool_smsac_uuid', true);
		if( $uuid ) {
			echo "<p><strong>Registered Device ID:</strong> {$uuid} <small><a href=\"{$link}&device_remove=1\">Remove</a></small></p>";
		}
echo <<<HTML

<a href="https://play.google.com/store/apps/details?id=com.trokis.indavaosmsac" target="_blank">
  <img alt="Get it on Google Play"
       src="https://developer.android.com/images/brand/en_generic_rgb_wo_60.png" />
</a>
		</div>
HTML;
	}
	
}

