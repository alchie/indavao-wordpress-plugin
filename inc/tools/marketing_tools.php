<?php

class InDavaoMarketing extends Custom_Admin_Page {
	
	public $id = 'indavao_marketing_tools';
	public $title = 'Marketing Tools';
	public $menu_name = 'Marketing Tools';
	public $permission = 'read';
	public $icon = 'marketing.png';
	public $priority = '300';
	
	function admin_page() {
		global $plugin_dir;
echo <<<HTML
		<div class="wrap" style="padding-top:20px;">

<div class="indavao-my-network theme-browser rendered">
	<div class="themes">
HTML;

$marketing_tools = array(
	array(
		'id' => 'smsac',
		'name' => 'SMS All Contacts',
		'image' => plugins_url($plugin_dir."/images/tool_smsac.png")
	),
	array(
		'id' => 'ssmss',
		'name' => 'Sequential SMS Sender',
		'image' => plugins_url($plugin_dir."/images/tool_ssmss.png")
	),
	array(
		'id' => 'fbacf',
		'name' => 'Facebook Auto Chat Friends',
		'image' => plugins_url($plugin_dir."/images/tool_fbacf.png")
	),
	array(
		'id' => 'fbgaj',
		'name' => 'Facebook Group Auto Joiner',
		'image' => plugins_url($plugin_dir."/images/tool_fbgaj.png")
	),
	array(
		'id' => 'fbgap',
		'name' => 'Facebook Group Auto Poster',
		'image' => plugins_url($plugin_dir."/images/tool_fbgap.png")
	),
	array(
		'id' => 'fbpal',
		'name' => 'Facebook Page Auto Liker',
		'image' => plugins_url($plugin_dir."/images/tool_fbpal.png")
	),
	array(
		'id' => 'fbpap',
		'name' => 'Facebook Page Auto Poster',
		'image' => plugins_url($plugin_dir."/images/tool_fbpap.png")
	),
);

foreach( $marketing_tools as $tool ) {
	$screenshot = ($tool['image']!='')?"<img src=\"{$tool['image']}\" alt=\"\">":'';
	$image_class = ($tool['image']!='')?"":'blank';
	$button_url = admin_url('admin.php?page=' . $this->id . '_' . $tool['id']);
	
echo <<<HTML
<div class="theme">
<div class="theme-screenshot {$image_class}">{$screenshot}</div>
<span class="more-details">{$tool['name']}</span>
<h3 class="theme-name">{$tool['name']} </h3>
<div class="theme-actions">
	<a class="button button-primary" href="{$button_url}">Learn More</a>
</div></div>
HTML;
}

echo <<<HTML
	</div>
<br class="clear"></div>

		</div>
HTML;
	}
}

