<?php
require('inc/content/main.php');
require('inc/content/establishments.php');
require('inc/content/realestate.php');
require('inc/content/jobs.php');
require('inc/content/cars.php');
require('inc/content/motorcycles.php');
require('inc/content/events.php');
require('inc/content/usermetas.php');
require('inc/content/backendapi.php');

$establishments = new InDavaoEstablishments;
$establishments->init();

$realestate = new InDavaoRealestate;
$realestate->init();

$jobs = new InDavaoJobs;
$jobs->init();

$cars = new InDavaoCars;
$cars->init();

$motorcycles = new InDavaoMotorcycles;
$motorcycles->init();

$events = new InDavaoEvents;
$events->init();
