<?php

require('inc/management/management.php');
require('inc/management/facebook_options.php');
require('inc/management/squeeze_pages.php');
require('inc/management/registration.php');
require('inc/management/mlm.php'); 
require('inc/management/teams.php');
require('inc/management/facebook_groups.php');
require('inc/management/encryption_key.php');


$management = new InDavaoManagement();
$squeeze_pages = new InDavaoManagementSqueezePages($management);
$facebook_options = new InDavaoManagementFacebookOptions($management);
$registration = new InDavaoManagementRegistration($management);
$mlm = new InDavaoManagementMLM($management);
$teams = new InDavaoManagementTeams( $management );
$fb_groups = new InDavaoManagementFacebookGroups( $management );
$encryption_key = new InDavaoManagementEncryptionKey( $management );

//require('inc/management/my_settings.php');
//$my_settings = new InDavaoManagementMySettings();
