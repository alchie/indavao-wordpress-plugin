<?php

// my network
require('inc/networking/my_network.php');
require('inc/networking/network_tree.php');

// network manager
require('inc/networking/manager_network.php');
require('inc/networking/manager_network_tree.php');

$my_network = new InDavaoNetworkingMyNetwork();
$network_tree = new InDavaoNetworkingMLMTree( $my_network );

$manager = new InDavaoNetworkingNetworkManager();
$manager_nt = new InDavaoNetworkingNetworkTreeManager($manager);
